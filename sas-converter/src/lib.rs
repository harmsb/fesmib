use bytes::{Bytes, BufMut};
use prost::*;
use sas_proto::messages::*;
use sas_serial::*;
use sas_proto::messages::sas_request::Request;


pub struct ProtoMessageFactory {}
impl ProtoMessageFactory {
    pub fn parse_progressive_level(src: u8) -> get_handpay_information_response::Level {
        match src {
            0 => get_handpay_information_response::Level::NonProgressiveLevel(0),
            1..=0x20 =>  get_handpay_information_response::Level::ProgressiveLevel(src as u32),
            _ => get_handpay_information_response::Level::NonProgressiveLevel(src as i32),
        }
    }

    // 0x1B
    pub fn process_get_handpay_information_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;
        let _poll = src.get_u8();

        let response = sas_response::Response::GetHandpayInformationResponse(GetHandpayInformationResponse {
            progressive_group: src.get_u8() as u32,
            level: Some(Self::parse_progressive_level(src.get_u8())),
            amount:  src.get_bcd(5),
            partial_pay: src.get_bcd(2) as u32,
            reset_type: src.get_u8() as i32,
        });

        SasResponse { machine_id, response: Some(response) }
    }

    pub fn ascii_to_payback(src: &[u8]) -> u32 {
        (src[0] - b'0') as u32 * 1000 +
        (src[1] - b'0') as u32 * 100 +
        (src[2] - b'0') as u32 * 10 +
        (src[3] - b'0') as u32
    }

    // 0x1F
    pub fn process_get_machine_info_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;
        let _poll = src.get_u8();

        let response = sas_response::Response::GetMachineInfoResponse(GetMachineInfoResponse {
            game_id: src.get_ascii(2),
            additional_id: src.get_ascii(3),
            denomination: src.get_u8() as i32,
            max_bet: src.get_u8() as u32,
            progressive_group: src.get_u8() as u32,
            game_options: src.get_u16_le() as u32,
            paytable_id: src.get_ascii(6),
            base_payback: Self::ascii_to_payback(src.get_ascii(4).as_slice()),
        });

        SasResponse { machine_id, response: Some(response) }
    }

    // 0x2F
    pub fn process_get_meter_values_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;
        let _poll = src.get_u8();

        let mut len = src.get_u8() as usize;

        let mut get_meter_values_response = GetMeterValuesResponse {
            game_number: src.get_bcd(2) as u32,
            meter_values: vec![]
        };

        len -= 2;
        while len > 0 {
            let meter_code = src.get_u8() as i32;
            let meter_len = METER_SIZES[meter_code as usize];
            let meter_value = src.get_bcd(meter_len);

            get_meter_values_response.meter_values.push(get_meter_values_response::MeterValue {
                meter_code,
                meter_value
            });

            len -= meter_len + 1;
        }

        SasResponse { machine_id, response: Some(sas_response::Response::GetMeterValuesResponse(get_meter_values_response)) }
    }

    // 0x53
    pub fn process_get_game_configuration_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;

        src.advance(1);     // Advance over poll ID
        //let poll = src.get_u8();

        SasResponse { 
            machine_id, 
            response: Some(sas_response::Response::GetGameConfigurationResponse(GetGameConfigurationResponse {
                game_number: src.get_bcd(2) as u32,
                game_id: src.get_ascii(2),
                additional_id: src.get_ascii(3),
                denomination: src.get_u8() as i32,
                max_bet: src.get_u8() as u32,
                progressive_group: src.get_u8() as u32,
                game_options: src.get_u16_le() as u32,
                paytable_id: src.get_ascii(6),
                base_payback:Self::ascii_to_payback(src.get_ascii(4).as_slice())
            })) }
    }

    // 0x55
    pub fn process_get_selected_game_number_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;
        src.advance(1);     // Advance over poll ID

        SasResponse {
            machine_id,
            response: Some(sas_response::Response::GetSelectedGameNumberResponse(GetSelectedGameNumberResponse {
                game_number: src.get_bcd(2) as u32,
            })) }
    }

    // 0x84
    pub fn process_get_progressive_win_amount_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;
        src.advance(1);     // Advance over poll ID

        SasResponse {
            machine_id,
            response: Some(sas_response::Response::GetProgressiveWinAmountResponse(GetProgressiveWinAmountResponse {
                group: src.get_u8() as u32,
                progressive_level: Some(ProgressiveLevel {
                    level: src.get_u8() as u32,
                    amount: src.get_bcd(5)
                })
            })) }
    }

    // 0x87
    pub fn process_get_progressive_win_amounts_response(mut src: impl SasSerialMessageReader) -> SasResponse {
        let machine_id = src.get_u8() as u32;
        src.advance(1);     // Advance over poll ID

        let group = src.get_u8() as u32;
        let num_levels = src.get_u8() as usize;
        let progressive_levels: Vec<ProgressiveLevel> = [0..num_levels].iter().map(|_| ProgressiveLevel {
            level: src.get_u8() as u32,
            amount: src.get_bcd(5)
        }).collect();

        SasResponse {
            machine_id,
            response: Some(sas_response::Response::GetProgressiveWinAmountsResponse(GetProgressiveWinAmountsResponse {group, progressive_levels}))
        }
    }

    pub fn proto_response_factory(src: Vec<u8>) -> Option<SasResponse> {
        let long_poll = src.get_long_poll();
        let buf = Bytes::from(src);

        match long_poll {
            None => None,
            Some(poll) => match poll {
                LongPollId::SendHandpayInformation => Some(Self::process_get_handpay_information_response(buf)),
                LongPollId::SendGamingMachineIdAndInformation => Some(Self::process_get_machine_info_response(buf)),
                LongPollId::SendSelectedMetersForGameN => Some(Self::process_get_meter_values_response(buf)),
                LongPollId::SendGameNConfiguration => Some(Self::process_get_game_configuration_response(buf)),
                LongPollId::SendSelectedGameNumber => Some(Self::process_get_selected_game_number_response(buf)),
                LongPollId::SendProgressiveWinAmount => Some(Self::process_get_progressive_win_amount_response(buf)),
                LongPollId::SendMultipleSasProgressiveWin => Some(Self::process_get_progressive_win_amounts_response(buf)),
                _ => None
            }
        }
    }
}

#[cfg(test)]
pub mod proto_message_factory_tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    // use crate::sas::proto::messages::sas_response::Response;
    // use crate::sas::proto::messages::get_handpay_information_response::Level;
    use sas_proto::messages::sas_response::Response;
    use sas_proto::messages::get_handpay_information_response::Level;
    use sas_proto::messages::get_meter_values_response::MeterValue;

    const EGM_ID: u8 = 0x42;

    #[test]
    fn test_bcd() {
        let sas: Vec<u8> = vec![self::EGM_ID].with_bcd(9999, 2).to_vec();
        println!("{:?}", sas);
    }

    #[test]
    fn test_get_handpay_info_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendHandpayInformation)
            .with_u8(13)                    // Group 0x12
            .with_u8(0x80)                    // Progressive level 1
            .with_bcd(1122, 5)      // Amount
            .with_bcd(3344, 2)      // Partial pay
            .with_u8(0)                     // Reset Id
            .with_slice(vec![10;0].as_slice())  // Reserved
            .with_crc().to_vec();

        println!("{:?}", sas);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        println!("{:?}", proto);

        if let Response::GetHandpayInformationResponse(response) = proto.response.expect("Expected GetHandpayInformationResponse") {
            assert_eq!(response.progressive_group, 13, "progressive_group mismatch");

            if let Level::NonProgressiveLevel(l) = response.level.unwrap() {
                assert_eq!(l, 0x80, "Level::NonProgressiveLevel(level) level mismatch ")
            } else {
                assert!(false, "Expected Level::NonProgressiveLevel(level)")
            }

            assert_eq!(response.amount , 1122);
            assert_eq!(response.partial_pay, 3344);
            assert_eq!(response.reset_type, 0);
        }
    }

    #[test]
    fn test_get_machine_info_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendGamingMachineIdAndInformation)
            .with_slice(b"AA")                  // Game ID
            .with_slice(b"BBB")                 // Additional ID
            .with_u8(0x80)                      // Denomination
            .with_u8(0x76)                      // Max Bet
            .with_u8(0x54)                      // Progressive group
            .with_u16(0x1234)                   // Game Options
            .with_slice(b"CCCCCC")              // Paytable ID
            .with_slice(b"1234")                // Base payback %
            .with_crc().to_vec();

        //println!("{:?}", sas);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        //println!("{:?}", proto);

        assert_eq!(proto.machine_id, EGM_ID as u32);

        if let Response::GetMachineInfoResponse(response) = proto.response.expect("Expected GetMachineInfoResponse") {
            //assert_eq!(response.game_number, 42);
            assert_eq!(response.game_id, b"AA".to_vec());
            assert_eq!(response.additional_id, b"BBB".to_vec());
            assert_eq!(response.denomination, 0x80);
            assert_eq!(response.max_bet, 0x76);
            assert_eq!(response.progressive_group, 0x54);
            assert_eq!(response.game_options, 0x1234);
            assert_eq!(response.paytable_id, b"CCCCCC".to_vec());
            assert_eq!(response.base_payback, 1234);
        }
    }

    #[test]
    fn test_get_meter_values_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendSelectedMetersForGameN)
            .with_u8(18)                       // Bytes to follow
            .with_bcd(42, 2)            // Game number

            .with_u8(1)                                     // Meter number (METER_CODE_TOTAL_COIN_OUT_CREDITS)
            .with_bcd(11223344, METER_SIZES[1])     // Meter value (BCD 4)

            .with_u8(13)                                    // Meter number (METER_CODE_TOTAL_SAS_CASHABLE_TICKET_IN_CENTS)
            .with_bcd(1122334455, METER_SIZES[13])  // Meter value (BCD 5)

            .with_u8(34)                                    // Meter number (METER_CODE_TOTAL_WON_CREDITS)
            .with_bcd(11223344, METER_SIZES[34])    // Meter value (BCD 4)

            .with_crc().to_vec();

        // println!("SAS serial message ({:?} bytes) = {:?}", sas.len(), sas);
        assert_eq!(sas.len() - 5, sas[2] as usize);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        assert_eq!(proto.machine_id, EGM_ID as u32);
        if let Response::GetMeterValuesResponse(get_meter_values_response) = proto.response.unwrap() {
            assert_eq!(get_meter_values_response.game_number, 42);
            assert_eq!(get_meter_values_response.meter_values, vec![
                MeterValue { meter_code: MeterCode::TotalCoinOutCredits.into(), meter_value: 11223344 },
                MeterValue { meter_code: MeterCode::TotalSasCashableTicketInCents.into(), meter_value: 1122334455 },
                MeterValue { meter_code: MeterCode::TotalWonCredits.into(), meter_value: 11223344 },
            ]);
        }
    }


    #[test]
    fn test_get_game_configuration_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendGameNConfiguration)
            .with_bcd(42, 2)            // Game Number
            .with_slice(b"AA")                  // Game N ID
            .with_slice(b"BBB")                 // Additional ID
            .with_u8(0x80)                      // Denomination
            .with_u8(0x60)                      // Max Bet
            .with_u8(0x40)                      // Progressive group
            .with_u16(0x1234)                   // Game Options
            .with_slice(b"CCCCCC")              // Paytable ID
            .with_slice(b"1234")                // Base payback %
            .with_crc().to_vec();

        println!("{:?}", sas);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        println!("{:?}", proto);

        assert_eq!(proto.machine_id, EGM_ID as u32);

        if let Response::GetGameConfigurationResponse(response) = proto.response.expect("Expected GetGameConfigurationResponse") {
            assert_eq!(response.game_number, 42);
            assert_eq!(response.game_id, b"AA".to_vec());
            assert_eq!(response.additional_id, b"BBB".to_vec());
            assert_eq!(response.denomination, 0x80);
            assert_eq!(response.max_bet, 0x60);
            assert_eq!(response.progressive_group, 0x40);
            assert_eq!(response.game_options, 0x1234);
            assert_eq!(response.paytable_id, b"CCCCCC".to_vec());
            assert_eq!(response.base_payback, 1234);
        }
    }
}


#[cfg(test)]
pub mod serial_message_factory_tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    const EGM_ID: u8 = 0x42;

    #[test]
    fn test_general_poll() {
        let general_poll = SasRequest { machine_id: self::EGM_ID as u32, request: Some(sas_request::Request::GeneralPoll(GeneralPoll { })) };

        let mut encoded = vec!();
        let _encoded_len = general_poll.encoded_len();
        general_poll.encode(&mut encoded).unwrap();
        println!("Encoded: {:?}", encoded);

        let msg = SasRequest::decode(encoded.as_slice()).unwrap();
        assert_eq!(msg.machine_id, self::EGM_ID as u32);

        let general_poll_serial = SerialMessageFactory::create_request(general_poll).unwrap();
        assert_eq!(general_poll_serial, vec![EGM_ID | 0x80]);
    }

    fn create_control_egm_request(egm_id: u8, machine_control_code: i32) -> SasRequest {
        SasRequest { machine_id: egm_id as u32, request: Some(sas_request::Request::ControlGamingMachineRequest(ControlGamingMachineRequest { machine_control_code })) }
    }

    fn test_control_egm_request(long_poll_id: LongPollId) {
        let control_code: u8 = long_poll_id.clone().into();
        let poll = create_control_egm_request(self::EGM_ID, control_code as i32);
        let poll_serial = SerialMessageFactory::create_request(poll).unwrap();
        assert_eq!(poll_serial.get_long_poll().unwrap(), long_poll_id);
        assert_eq!(poll_serial.get_egm_address().unwrap(), EGM_ID);
    }

    #[test]
    fn test_lp01_shut_down() {
        test_control_egm_request(LongPollId::Shutdown);
    }

    #[test]
    fn test_lp02_start_up() {
        test_control_egm_request(LongPollId::Startup);
    }

    #[test]
    fn test_lp03_sound_off() {
        test_control_egm_request(LongPollId::SoundOff);
    }

    #[test]
    fn test_lp04_sound_on() {
        test_control_egm_request(LongPollId::SoundOn);
    }

    #[test]
    fn test_lp05_reel_spin_or_game_play_sounds_disabled() {
        test_control_egm_request(LongPollId::ReelSpinOrGamePlaySoundsDisabled);
    }

    #[test]
    fn test_lp06_enable_bill_acceptor() {
        test_control_egm_request(LongPollId::EnableBillAcceptor);
    }

    #[test]
    fn test_lp07_disable_bill_acceptor() {
        test_control_egm_request(LongPollId::DisableBillAcceptor);
    }

    #[test]
    fn test_lp2f_send_selected_meters_for_game_n() {
        let poll = SasRequest {
            machine_id: self::EGM_ID as u32,
            request: Some(sas_request::Request::GetMeterValuesRequest(GetMeterValuesRequest {
                game_number: 1234,
                meter_codes: vec![MeterCode::CurrentCredits.into()] }))
        };
        let poll_clone = poll.clone();
        let poll_serial = SerialMessageFactory::create_request(poll).unwrap();

        println!("Serial: {:?}", poll_serial);

        let mut encoded = vec!();
        poll_clone.encode(&mut encoded).unwrap();
        println!("Encoded .proto: {:?}", encoded);

        let msg:SasRequest = SasRequest::decode(encoded.as_slice()).unwrap();
        assert_eq!(msg.machine_id, self::EGM_ID as u32);

        assert_eq!(poll_serial.get_long_poll().unwrap(), LongPollId::SendSelectedMetersForGameN);
        assert_eq!(poll_serial.get_egm_address().unwrap(), EGM_ID);
        assert_eq!(poll_serial[2], 0x03);
        assert_eq!(poll_serial[3], 0x12);
        assert_eq!(poll_serial[4], 0x34);
        assert_eq!(poll_serial[5] as i32, MeterCode::CurrentCredits.into());
    }

    #[test]
    fn test_encode() {
        let data: &[u8] = &[8, 66, 250, 2, 6, 8, 210, 9, 18, 1, 12];
        let msg: SasRequest = SasRequest::decode(data).unwrap();

        println!("{:?}", msg);

        assert_eq!(msg.machine_id, self::EGM_ID as u32);
    }
}

pub trait ProgressiveLevelConverter {
    fn with_progressive_level_data(&mut self, src: Vec<ProgressiveLevel>)  -> &mut Self;
}

impl ProgressiveLevelConverter for Vec<u8> {
    fn with_progressive_level_data(&mut self, src: Vec<ProgressiveLevel>)  -> &mut Self {
        self.put_u8((Self::PROGRESSIVE_LEVEL_AMOUNT_SIZE * src.len()) as u8);

        for progressive_level in src {
            self.put_u8(progressive_level.level as u8);
            self.put_slice(encode_bcd(progressive_level.amount, Self::PROGRESSIVE_LEVEL_AMOUNT_BCD_SIZE).as_slice());
        }
        self
    }
}

pub struct SerialMessageFactory {}

impl SerialMessageFactory {
    pub fn serial(egm_id: u8) -> Vec<u8> {
        vec![egm_id]
    }

    pub fn process_general_poll(egm_id: u8, _poll: GeneralPoll) -> Vec<u8> {
        Self::serial(egm_id | 0x80)
    }

    pub fn create_id_only_poll(egm_id: u8, poll_id: LongPollId) -> Vec<u8> {
        Self::serial(egm_id).with_long_poll(poll_id).with_crc().to_vec()
    }

    /* Section 7: Long polls
     * - 7.4: Enable/Disable Long Polls
     *     - Table 7.4A: Polls 0x00-0x07, 0x0A, 0x0B
     *
     * Allows host to enable/disable some aspects of the EGM.
     */
    pub fn process_control_gaming_machine_request(egm_id: u8, poll: ControlGamingMachineRequest) -> Vec<u8> {
        Self::serial(egm_id).with_u8(poll.machine_control_code as u8).with_crc().to_vec()
    }

    // Poll 0x1B, Section 7.8, page 7-12
    pub fn process_get_handpay_information_request(egm_id: u8, _poll:GetHandpayInformationRequest) -> Vec<u8> {
        Self::create_id_only_poll(egm_id, LongPollId::SendHandpayInformation)
    }

    // Send Gaming Machine ID and Information: Poll 0x1F (Section 7.10, page 7-14)
    pub fn process_get_machine_info_request(egm_id: u8, _poll:GetMachineInfoRequest) -> Vec<u8> {
        Self::create_id_only_poll(egm_id, LongPollId::SendGamingMachineIdAndInformation)
    }

    // Poll 0x2F, section 7.3 page 7-3
    pub fn process_get_meter_values_request(egm_id: u8, poll:GetMeterValuesRequest)-> Vec<u8> {
        let meter_codes:Vec<u8> = poll.meter_codes.iter().map(|m| *m as u8).collect();
        Self::serial(egm_id)
            .with_long_poll(LongPollId::SendSelectedMetersForGameN)
            .with_u8(poll.meter_codes.len() as u8  + 2)
            .with_bcd(poll.game_number as u64, 2)
            .with_slice(&meter_codes)
            .with_crc().to_vec()
    }

    // Poll 0x53, Section 7.6.5, page 7-9
    pub fn process_get_game_configuration_request(egm_id: u8, poll:GetGameConfigurationRequest)-> Vec<u8> {
        Self::serial(egm_id).with_long_poll(LongPollId::SendGameNConfiguration).with_bcd(poll.game_number as u64, 2).with_crc().to_vec()
    }

    // Poll 0x53, Section 7.6.5, page 7-9
    pub fn process_get_selected_game_number_request(egm_id: u8, _poll:GetSelectedGameNumberRequest) -> Vec<u8> {
        Self::create_id_only_poll(egm_id, LongPollId::SendSelectedGameNumber)
    }

    // Send Progressive Win Amount: Poll 0x84, Section 10.4.2, page 10-2
    pub fn process_get_progressive_win_amount_request(egm_id: u8, _poll:GetProgressiveWinAmountRequest) -> Vec<u8> {
        Self::create_id_only_poll(egm_id, LongPollId::SendProgressiveWinAmount)
    }

    // Send Progressive Win Amounts: Poll 0x87, Section 10.4.3, page 10-4
    pub fn process_get_progressive_win_amounts_request(egm_id: u8, _poll:GetProgressiveWinAmountsRequest) -> Vec<u8> {
        Self::create_id_only_poll(egm_id, LongPollId::SendMultipleSasProgressiveWin)
    }

    /* SECTION 10 -- PROGRESSIVES */

    // Multiple Level Broadcast: Poll 0x86, Section 10.1b, page 10-1
    pub fn process_progressive_level_broadcast_request(egm_id: u8, poll:ProgressiveLevelBroadcastRequest)-> Vec<u8> {
        Self::serial(egm_id).with_long_poll(LongPollId::ReceiveMultipleProgressiveLevels)
            .with_bcd(poll.group as u64, 2)
            .with_progressive_level_data(poll.progressive_levels)
            .with_crc().to_vec()
    }

    pub fn create_request(src: SasRequest) -> Option<Vec<u8>> {
        let egm_id = src.machine_id as u8;
        match src.request {
            None => None,
            Some(r) => match r {
                Request::GeneralPoll(p) => Some(Self::process_general_poll(egm_id, p)),
                Request::ControlGamingMachineRequest(p) => Some(Self::process_control_gaming_machine_request(egm_id, p)),
                Request::GetMeterValuesRequest(p) => Some(Self::process_get_meter_values_request(egm_id, p)),
                Request::GetGameConfigurationRequest(p) => Some(Self::process_get_game_configuration_request(egm_id, p)),
                Request::GetSelectedGameNumberRequest(p) => Some(Self::process_get_selected_game_number_request(egm_id, p)),
                Request::GetHandpayInformationRequest(p) => Some(Self::process_get_handpay_information_request(egm_id, p)),
                Request::GetMachineInfoRequest(p) => Some(Self::process_get_machine_info_request(egm_id, p)),
                Request::ProgressiveLevelBroadcastRequest(p) => Some(Self::process_progressive_level_broadcast_request(egm_id, p)),
                Request::GetProgressiveWinAmountRequest(p) => Some(Self::process_get_progressive_win_amount_request(egm_id, p)),
                _ => None
            },
        }
    }
}