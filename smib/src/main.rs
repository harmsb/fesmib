fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
pub mod main_app_tests {
    use prost::*;

    use sas_proto::messages::sas_response::Response;
    use sas_proto::messages::get_handpay_information_response::Level;
    use sas_proto::messages::get_meter_values_response::MeterValue;
    use sas_serial::{SasSerialMessageBuilder, LongPollId, METER_SIZES, SasSerial};
    use sas_converter::{ProtoMessageFactory, SerialMessageFactory};
    use sas_proto::messages::{MeterCode, SasRequest, sas_request, GeneralPoll, ControlGamingMachineRequest, GetMeterValuesRequest};

    const EGM_ID: u8 = 0x42;

    #[test]
    fn test_bcd() {
        let sas: Vec<u8> = vec![self::EGM_ID].with_bcd(9999, 2).to_vec();
        println!("{:?}", sas);
    }

    #[test]
    fn test_get_handpay_info_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendHandpayInformation)
            .with_u8(13)                    // Group 0x12
            .with_u8(0x80)                    // Progressive level 1
            .with_bcd(1122, 5)      // Amount
            .with_bcd(3344, 2)      // Partial pay
            .with_u8(0)                     // Reset Id
            .with_slice(vec![10;0].as_slice())  // Reserved
            .with_crc().to_vec();

        println!("{:?}", sas);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        println!("{:?}", proto);

        if let Response::GetHandpayInformationResponse(response) = proto.response.expect("Expected GetHandpayInformationResponse") {
            assert_eq!(response.progressive_group, 13, "progressive_group mismatch");

            if let Level::NonProgressiveLevel(l) = response.level.unwrap() {
                assert_eq!(l, 0x80, "Level::NonProgressiveLevel(level) level mismatch ")
            } else {
                assert!(false, "Expected Level::NonProgressiveLevel(level)")
            }

            assert_eq!(response.amount , 1122);
            assert_eq!(response.partial_pay, 3344);
            assert_eq!(response.reset_type, 0);
        }
    }

    #[test]
    fn test_get_machine_info_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendGamingMachineIdAndInformation)
            .with_slice(b"AA")                  // Game ID
            .with_slice(b"BBB")                 // Additional ID
            .with_u8(0x80)                      // Denomination
            .with_u8(0x76)                      // Max Bet
            .with_u8(0x54)                      // Progressive group
            .with_u16(0x1234)                   // Game Options
            .with_slice(b"CCCCCC")              // Paytable ID
            .with_slice(b"1234")                // Base payback %
            .with_crc().to_vec();

        //println!("{:?}", sas);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        //println!("{:?}", proto);

        assert_eq!(proto.machine_id, EGM_ID as u32);

        if let Response::GetMachineInfoResponse(response) = proto.response.expect("Expected GetMachineInfoResponse") {
            //assert_eq!(response.game_number, 42);
            assert_eq!(response.game_id, b"AA".to_vec());
            assert_eq!(response.additional_id, b"BBB".to_vec());
            assert_eq!(response.denomination, 0x80);
            assert_eq!(response.max_bet, 0x76);
            assert_eq!(response.progressive_group, 0x54);
            assert_eq!(response.game_options, 0x1234);
            assert_eq!(response.paytable_id, b"CCCCCC".to_vec());
            assert_eq!(response.base_payback, 1234);
        }
    }

    #[test]
    fn test_get_meter_values_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendSelectedMetersForGameN)
            .with_u8(18)                       // Bytes to follow
            .with_bcd(42, 2)            // Game number

            .with_u8(1)                                     // Meter number (METER_CODE_TOTAL_COIN_OUT_CREDITS)
            .with_bcd(11223344, METER_SIZES[1])     // Meter value (BCD 4)

            .with_u8(13)                                    // Meter number (METER_CODE_TOTAL_SAS_CASHABLE_TICKET_IN_CENTS)
            .with_bcd(1122334455, METER_SIZES[13])  // Meter value (BCD 5)

            .with_u8(34)                                    // Meter number (METER_CODE_TOTAL_WON_CREDITS)
            .with_bcd(11223344, METER_SIZES[34])    // Meter value (BCD 4)

            .with_crc().to_vec();

        // println!("SAS serial message ({:?} bytes) = {:?}", sas.len(), sas);
        assert_eq!(sas.len() - 5, sas[2] as usize);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        assert_eq!(proto.machine_id, EGM_ID as u32);
        if let Response::GetMeterValuesResponse(get_meter_values_response) = proto.response.unwrap() {
            assert_eq!(get_meter_values_response.game_number, 42);
            assert_eq!(get_meter_values_response.meter_values, vec![
                MeterValue { meter_code: MeterCode::TotalCoinOutCredits.into(), meter_value: 11223344 },
                MeterValue { meter_code: MeterCode::TotalSasCashableTicketInCents.into(), meter_value: 1122334455 },
                MeterValue { meter_code: MeterCode::TotalWonCredits.into(), meter_value: 11223344 },
            ]);
        }
    }


    #[test]
    fn test_get_game_configuration_response() {
        let sas: Vec<u8> = vec![self::EGM_ID]
            .with_long_poll(LongPollId::SendGameNConfiguration)
            .with_bcd(42, 2)            // Game Number
            .with_slice(b"AA")                  // Game N ID
            .with_slice(b"BBB")                 // Additional ID
            .with_u8(0x80)                      // Denomination
            .with_u8(0x60)                      // Max Bet
            .with_u8(0x40)                      // Progressive group
            .with_u16(0x1234)                   // Game Options
            .with_slice(b"CCCCCC")              // Paytable ID
            .with_slice(b"1234")                // Base payback %
            .with_crc().to_vec();

        println!("{:?}", sas);

        let proto = ProtoMessageFactory::proto_response_factory(sas).unwrap();

        println!("{:?}", proto);

        assert_eq!(proto.machine_id, EGM_ID as u32);

        if let Response::GetGameConfigurationResponse(response) = proto.response.expect("Expected GetGameConfigurationResponse") {
            assert_eq!(response.game_number, 42);
            assert_eq!(response.game_id, b"AA".to_vec());
            assert_eq!(response.additional_id, b"BBB".to_vec());
            assert_eq!(response.denomination, 0x80);
            assert_eq!(response.max_bet, 0x60);
            assert_eq!(response.progressive_group, 0x40);
            assert_eq!(response.game_options, 0x1234);
            assert_eq!(response.paytable_id, b"CCCCCC".to_vec());
            assert_eq!(response.base_payback, 1234);
        }
    }

    #[test]
    fn test_general_poll() {
        let general_poll = SasRequest { machine_id: self::EGM_ID as u32, request: Some(sas_request::Request::GeneralPoll(GeneralPoll { })) };

        let mut encoded = vec!();
        let _encoded_len = general_poll.encoded_len();
        general_poll.encode(&mut encoded).unwrap();
        println!("Encoded: {:?}", encoded);

        let msg = SasRequest::decode(encoded.as_slice()).unwrap();
        assert_eq!(msg.machine_id, self::EGM_ID as u32);

        let general_poll_serial = SerialMessageFactory::create_request(general_poll).unwrap();
        assert_eq!(general_poll_serial, vec![EGM_ID | 0x80]);
    }

    fn create_control_egm_request(egm_id: u8, machine_control_code: i32) -> SasRequest {
        SasRequest { machine_id: egm_id as u32, request: Some(sas_request::Request::ControlGamingMachineRequest(ControlGamingMachineRequest { machine_control_code })) }
    }

    fn test_control_egm_request(long_poll_id: LongPollId) {
        let control_code: u8 = long_poll_id.clone().into();
        let poll = create_control_egm_request(self::EGM_ID, control_code as i32);
        let poll_serial = SerialMessageFactory::create_request(poll).unwrap();
        assert_eq!(poll_serial.get_long_poll().unwrap(), long_poll_id);
        assert_eq!(poll_serial.get_egm_address().unwrap(), EGM_ID);
    }

    #[test]
    fn test_lp01_shut_down() {
        test_control_egm_request(LongPollId::Shutdown);
    }

    #[test]
    fn test_lp02_start_up() {
        test_control_egm_request(LongPollId::Startup);
    }

    #[test]
    fn test_lp03_sound_off() {
        test_control_egm_request(LongPollId::SoundOff);
    }

    #[test]
    fn test_lp04_sound_on() {
        test_control_egm_request(LongPollId::SoundOn);
    }

    #[test]
    fn test_lp05_reel_spin_or_game_play_sounds_disabled() {
        test_control_egm_request(LongPollId::ReelSpinOrGamePlaySoundsDisabled);
    }

    #[test]
    fn test_lp06_enable_bill_acceptor() {
        test_control_egm_request(LongPollId::EnableBillAcceptor);
    }

    #[test]
    fn test_lp07_disable_bill_acceptor() {
        test_control_egm_request(LongPollId::DisableBillAcceptor);
    }

    #[test]
    fn test_lp2f_send_selected_meters_for_game_n() {
        let poll = SasRequest {
            machine_id: self::EGM_ID as u32,
            request: Some(sas_request::Request::GetMeterValuesRequest(GetMeterValuesRequest {
                game_number: 1234,
                meter_codes: vec![MeterCode::CurrentCredits.into()] }))
        };
        let poll_clone = poll.clone();
        let poll_serial = SerialMessageFactory::create_request(poll).unwrap();

        println!("Serial: {:?}", poll_serial);

        let mut encoded = vec!();
        poll_clone.encode(&mut encoded).unwrap();
        println!("Encoded .proto: {:?}", encoded);

        let msg:SasRequest = SasRequest::decode(encoded.as_slice()).unwrap();
        assert_eq!(msg.machine_id, self::EGM_ID as u32);

        assert_eq!(poll_serial.get_long_poll().unwrap(), LongPollId::SendSelectedMetersForGameN);
        assert_eq!(poll_serial.get_egm_address().unwrap(), EGM_ID);
        assert_eq!(poll_serial[2], 0x03);
        assert_eq!(poll_serial[3], 0x12);
        assert_eq!(poll_serial[4], 0x34);
        assert_eq!(poll_serial[5] as i32, MeterCode::CurrentCredits.into());
    }

    #[test]
    fn test_encode() {
        let data: &[u8] = &[8, 66, 250, 2, 6, 8, 210, 9, 18, 1, 12];
        let msg: SasRequest = SasRequest::decode(data).unwrap();

        println!("{:?}", msg);

        assert_eq!(msg.machine_id, self::EGM_ID as u32);
    }
}
