pub mod messages {
    /// Section 2:
    /// - 2.2.1: Poll 0x00
    ///
    /// Sent by the host to query EGM status
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GeneralPoll {}

    /// Section 7:
    /// - 7.4: Poll N/A
    ///
    /// Sent by the EGM to explicitly NACK a request.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct NackResponse {}

    /// Section 7:
    /// - 7.4: Poll N/A
    ///
    /// Sent by the EGM to explicitly ACK a request.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AckResponse {}

// =====================================================================================================================
// Section 6: ROM Signature
// =====================================================================================================================

    /// Section 6: ROM Signature
    /// - Table 6.2A: Poll 0x21
    ///
    /// Sent by the host to initiate a CRC16 signature over an EGM's ROM
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct RomSignatureRequest {
        /// Seed to use for the CRC16 calculation.
        #[prost(uint32, tag = "1")]
        pub seed: u32,
    }

    /// Section 6: ROM Signature
    /// - 6.2
    ///     - Table 6.2B: Poll 0x21
    ///
    /// Sent by the EGM in response to the first *GeneralPoll* a CRC16 signature over an EGM's ROM
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct RomSignatureResponse {
        #[prost(uint32, tag = "1")]
        pub crc_16: u32,
    }

// @exclude
// =====================================================================================================================
// Section 7: Long polls
// =====================================================================================================================

    /// Section 7: Long polls
    /// - 7.3
    ///     - Table 7.3A: Poll 0x2F
    ///
    /// Sent by the host to query meter values.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetMeterValuesRequest {
        /// Number of the game to query, 0 for machine-wide.
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Meters to read (MaxCount:16)
        #[prost(enumeration = "MeterCode", repeated, tag = "2")]
        pub meter_codes: ::std::vec::Vec<i32>,
    }

    /// Section 7: Long polls
    /// - 7.3
    ///     - Table 7.3B: Poll 0x2F
    ///
    /// Sent by the EGM in response to *GetMeterValuesRequest*
    /// Returns a set of *MeterValue*s for the requested meters.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetMeterValuesResponse {
        /// Number of the game to query, 0 for machine-wide.
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Values of the requested meters (MaxCount:16)
        #[prost(message, repeated, tag = "2")]
        pub meter_values: ::std::vec::Vec<get_meter_values_response::MeterValue>,
    }

    pub mod get_meter_values_response {
        /// Value of a meter as a <*MeterCode*, uint64> pair.
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct MeterValue {
            /// Code for the meter
            #[prost(enumeration = "super::MeterCode", tag = "1")]
            pub meter_code: i32,
            /// Meter's value
            #[prost(uint64, tag = "2")]
            pub meter_value: u64,
        }
    }
// @exclude 7.4 Enable/Disable long-polls (polls 0x00-0x07, 0x0A, 0x0B)

    /// Section 7: Long polls
    /// - 7.4: Enable/Disable Long Polls
    ///     - Table 7.4A: Polls 0x00-0x07, 0x0A, 0x0B
    ///
    /// Allows host to enable/disable some aspects of the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ControlGamingMachineRequest {
        #[prost(enumeration = "control_gaming_machine_request::MachineControlCode", tag = "1")]
        pub machine_control_code: i32,
    }

    pub mod control_gaming_machine_request {
        /// Defines the action to control on the EGM
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum MachineControlCode {
            Invalid = 0,
            /// Shutdown the EGM
            Shutdown = 1,
            /// Start-up the EGM
            Startup = 2,
            /// Turn off EGM sounds
            SoundOff = 3,
            /// Turn on EGM sounds
            SoundOn = 4,
            /// Disable game-play sounds
            ReelSpinOrGamePlaySoundsDisabled = 5,
            /// Enable the bill acceptor
            EnableBillAcceptor = 6,
            /// Disable the bill acceptor
            DisableBillAcceptor = 7,
            /// Enter maintenance mode
            EnterMaintenanceMode = 10,
            /// Exit maintenance mode
            ExitMaintenanceMode = 11,
        }
    }

    /// Section 7: Long polls
    /// - 7.5 Configure Bill Denominations
    ///     - Table 7.5: Poll 0x08
    ///
    /// Allows host to enable/disable bill denominations.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ConfigureBillDenominationsRequest {
        /// Bills of listed denominations are enabled, all others are disabled.
        #[prost(uint32, tag = "1")]
        pub bill_denominations_bit_flags: u32,
        #[prost(bool, tag = "2")]
        pub bill_acceptor_enabled_after_bill: bool,
    }

    /// Section 7: Long polls
    /// - 7.6.1 Enable/disable Game N
    ///     - Table 7.6.1: Poll 0x09
    ///
    /// Allows host to enable/disable a specific game on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetGameEnableStateRequest {
        /// Number of the game to enable/disable
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// New state for the game
        #[prost(bool, tag = "2")]
        pub is_enabled: bool,
    }

    /// Section 7: Long polls
    /// - 7.6.2 Send Total Hand Paid Cancelled Credits
    ///     - Table 7.6.2a: Poll 0x2D
    ///
    /// Gets the total hand-paid cancelled credits for a specific game on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetTotalHandPaidCancelledCreditsRequest {
        /// Number of the game to query
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// Section 7: Long polls
    /// - 7.6.2 Send Total Hand Paid Cancelled Credits
    ///     - Table 7.6.2b: Poll 0x2D
    ///
    /// Gets the total hand-paid cencelled credits for a specific game on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetTotalHandPaidCancelledCreditsResponse {
        /// Number of the game being queried
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Total cancelled hand-paid cancelled credits
        #[prost(uint64, tag = "2")]
        pub total_hand_paid_cancelled_credits: u64,
    }

    /// Section 7: Long polls
    /// - 7.6.3 Send Number of Games Implemented
    ///     - Poll 0x51
    ///
    /// Gets the number of games implemented on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetNumberOfGamesImplementedRequest {}

    /// Section 7: Long polls
    /// - 7.6.3 Send Number of Games Implemented
    ///     - Table 7.6.3a: Poll 0x51
    ///
    /// Gets the number of games implemented on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetNumberOfGamesImplementedResponse {
        /// number of games implemented on the EGM.
        #[prost(uint32, tag = "1")]
        pub number_of_games_implemented: u32,
    }

    /// Section 7: Long polls
    /// - 7.6.5 Send Game N Configuration
    ///     - Table 7.6.5a: Poll 0x53
    ///
    /// Gets information about a specific game on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetGameConfigurationRequest {
        /// Number of the game to query
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// Section 7: Long polls
    /// - 7.6.5 Send Game N Configuration
    ///     - Table 7.6.5b: Poll 0x53
    ///
    /// Gets information about a specific game on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetGameConfigurationResponse {
        /// Number of game being reported
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Game ID from Table C-1 (MaxLength:2)
        #[prost(bytes, tag = "2")]
        pub game_id: std::vec::Vec<u8>,
        /// Additional game ID in ASCII (MaxLength:3)
        #[prost(bytes, tag = "3")]
        pub additional_id: std::vec::Vec<u8>,
        /// Game denomination
        #[prost(enumeration = "Denomination", tag = "4")]
        pub denomination: i32,
        /// Max bet, 0xFF if bet is greater than 255
        #[prost(uint32, tag = "5")]
        pub max_bet: u32,
        /// Configured progressive group, zero for EDT, stand-alone, or non-progressive game
        #[prost(uint32, tag = "6")]
        pub progressive_group: u32,
        /// Bitmap of options for the game, see Table C-2
        #[prost(uint32, tag = "7")]
        pub game_options: u32,
        /// Paytable ID, in ASCII (MaxLength:6) -- see Table C-3
        #[prost(bytes, tag = "8")]
        pub paytable_id: std::vec::Vec<u8>,
        /// Base payback percentage, 4 digits of precision with an implied decimal point, (ie 1234 = 12.34%)
        #[prost(fixed32, tag = "9")]
        pub base_payback: u32,
    }

    /// Section 7: Long polls
    /// - 7.6.6 Send Selected Game Number
    ///     - Poll 0x55
    ///
    /// Gets the number of the EGM's selected game.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetSelectedGameNumberRequest {}

    /// Section 7: Long polls
    /// - 7.6.6 Send Selected Game Number
    ///     - Table 7.6.6: Poll 0x55
    ///
    /// Gets the number of the EGM's selected game.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetSelectedGameNumberResponse {
        /// EGM's currently selected game
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// Section 7: Long polls
    /// - 7.6.7 Send Selected Game Number
    ///     - Poll 0x56
    ///
    /// Gets the numbers of the games enabled on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnabledGameNumbersRequest {}

    /// Section 7: Long polls
    /// - 7.6.7 Send Selected Game Number
    ///     - Table 7.6.7: Poll 0x56
    ///
    /// Gets the numbers of the games enabled on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnabledGameNumbersResponse {
        /// Numbers of the games enabled on the EGM (MaxCount:32)
        #[prost(uint32, repeated, tag = "1")]
        pub game_numbers: ::std::vec::Vec<u32>,
    }

    /// Section 7: Long polls
    /// - 7.7 Send Games Played Since Last Power Up And Slot Door Closure
    ///     - Poll 0x18
    ///
    /// Get the number of the games played since last power up and slot door closure
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetGamesPlayedSinceLastPowerupRequest {}

    /// Section 7: Long polls
    /// - 7.7 Send Games Played Since Last Power Up And Slot Door Closure
    ///     - Table 7.7: Poll 0x18
    ///
    /// Get the number of the games played since last power up and slot door closure
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetGamesPlayedSinceLastPowerupResponse {
        /// number of the games played since last power up
        #[prost(uint32, tag = "1")]
        pub games_played_since_power_up: u32,
        /// number of the games played since slot door closure
        #[prost(uint32, tag = "2")]
        pub games_played_since_slot_door_closure: u32,
    }

    /// Section 7: Long polls
    /// - 7.8 Send Handpay Information
    ///     - Poll 0x1B
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetHandpayInformationRequest {}

    /// Section 7: Long polls
    /// - 7.8 Send Handpay Information
    ///     - Table 7.8: Poll 0x1B
    ///
    /// Provides information about the requested handpay, including:
    /// - Progressive group of the highest contributing progressive win for the handpay:
    ///     - Value 0x00 is used for stand-alone, non-progressive, or linked-progressive
    ///     - Values 0x01 - 0xFF are used for host-controlled progressive groups
    /// - Level may be either a normal progressive level or a meta-level for:
    ///     - Non-progressive win amount
    ///     - Non-progressive top-win amount (optional)
    ///     - Cancelled credits amount
    /// - Handpay amount
    /// - Partial paid amount
    /// - Reset type
    ///
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetHandpayInformationResponse {
        /// Progressive group of the highest contributing progressive win for the handpay.
        #[prost(uint32, tag = "1")]
        pub progressive_group: u32,
        /// Amount of the handpay
        #[prost(uint64, tag = "4")]
        pub amount: u64,
        /// Partial pay amount paid prior to handpay
        #[prost(uint32, tag = "5")]
        pub partial_pay: u32,
        /// Reset type
        #[prost(enumeration = "get_handpay_information_response::HandpayResetType", tag = "6")]
        pub reset_type: i32,
        #[prost(oneof = "get_handpay_information_response::Level", tags = "2, 3")]
        pub level: ::std::option::Option<get_handpay_information_response::Level>,
    }

    pub mod get_handpay_information_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum HandpayNonProgressiveLevel {
            /// Non-progressive win amount
            HandpayNonprogressiveLevelWinAmount = 0,
            /// Non-progressive top-win amount (optional)
            HandpayNonprogressiveLevelTopWinAmount = 64,
            /// Cancelled credits amount
            HandpayNonprogressiveLevelCancelledCreditAmount = 128,
        }

        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum HandpayResetType {
            Standard = 0,
            CreditMeter = 1,
        }

        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Level {
            /// Non-progressive meta-level
            #[prost(enumeration = "HandpayNonProgressiveLevel", tag = "2")]
            NonProgressiveLevel(i32),
            /// Normal progressive level
            #[prost(uint32, tag = "3")]
            ProgressiveLevel(u32),
        }
    }
// Section 7: Long polls
// - 7.9 Remote Handpay Reset
//     - Table 7.9a: Poll 0x94
//
// Host sends this message to remotely reset a handpay.

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct RemoteHandpayResetRequest {}

    /// Section 7: Long polls
    /// - 7.9 Remote Handpay Reset
    ///     - Table 7.9b: Poll 0x94
    ///
    /// Reports the result of remote handpay reset.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct RemoteHandpayResetResponse {
        /// Result of the handpay reset
        #[prost(enumeration = "remote_handpay_reset_response::Result", tag = "1")]
        pub result: i32,
    }

    pub mod remote_handpay_reset_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum Result {
            /// Success
            ResetSuccess = 0,
            /// Failure
            ResetFailed = 1,
            /// EGM was not in a handpay state.
            NotInHandpay = 2,
        }
    }

    /// Section 7: Long polls
    /// - 7.10 Send Gaming Machine ID and Information
    ///     - Poll 0x1F
    ///
    /// Query information about an EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetMachineInfoRequest {}

    /// Section 7: Long polls
    /// - 7.10 Send Gaming Machine ID and Information
    ///     - Table 7.10: Poll 0x1F
    ///
    /// Query information about an EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetMachineInfoResponse {
        /// Game ID in ASCII (MaxSize:2)
        #[prost(bytes, tag = "1")]
        pub game_id: std::vec::Vec<u8>,
        /// Additional Game ID in ASCII (MaxSize:3)
        #[prost(bytes, tag = "2")]
        pub additional_id: std::vec::Vec<u8>,
        /// EGM's accounting denomination
        #[prost(enumeration = "Denomination", tag = "3")]
        pub denomination: i32,
        /// Largest max bet for the EGM, 0xFF for values greater than 255.
        #[prost(uint32, tag = "4")]
        pub max_bet: u32,
        /// Configured progressive group
        #[prost(uint32, tag = "5")]
        pub progressive_group: u32,
        /// Bitmap of options for the game, see Table C-2
        #[prost(uint32, tag = "6")]
        pub game_options: u32,
        /// Paytable ID, in ASCII (MaxLength:6) -- see Table C-3
        #[prost(bytes, tag = "7")]
        pub paytable_id: std::vec::Vec<u8>,
        /// Base payback percentage, 4 digits of precision with an implied decimal point, (ie 1234 = 12.34%)
        #[prost(fixed32, tag = "8")]
        pub base_payback: u32,
    }

    /// Section 7: Long polls
    /// - 7.11 Send Last Bill Accepted Information
    ///     - Poll 0x48
    ///
    /// Query information about the last bill accepted.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetLastAcceptedBillInfoRequest {}

    /// Section 7: Long polls
    /// - 7.11 Send Last Bill Accepted Information
    ///     - Table 7.11: Poll 0x48
    ///
    /// Query information about the last bill accepted.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetLastAcceptedBillInfoResponse {
        /// Country code of the bill
        #[prost(enumeration = "BillAcceptorCountryCode", tag = "1")]
        pub country_code: i32,
        /// Bill denomination
        #[prost(enumeration = "Denomination", tag = "2")]
        pub denomination: i32,
        /// Count of the number of bills of this type accepted.
        #[prost(uint32, tag = "3")]
        pub bill_meter: u32,
    }

    /// Section 7: Long polls
    /// - 7.12 Send Card Information
    ///     - Poll 0x8E
    ///
    /// Query information about the cards.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCardInformationRequest {}

    /// Section 7: Long polls
    /// - 7.11 Send Card Information
    ///     - Table 7.12a: Poll 0x8E
    ///
    /// Query information about the cards.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCardInformationResponse {
        /// Indicates if this is a dealt or final hand
        #[prost(enumeration = "get_card_information_response::HandType", tag = "1")]
        pub hand_type: i32,
        /// Cards in the hand. (MaxCount:8)
        #[prost(message, repeated, tag = "2")]
        pub cards: ::std::vec::Vec<get_card_information_response::Card>,
    }

    pub mod get_card_information_response {
        /// Describes the suit and rank of a card. (See Table 7.12b)
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct Card {
            /// Card's rank
            #[prost(enumeration = "card::Rank", tag = "1")]
            pub rank: i32,
            /// Card's suit
            #[prost(enumeration = "card::Suit", tag = "2")]
            pub suit: i32,
        }

        pub mod card {
            /// Card's suit
            #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
            #[repr(i32)]
            pub enum Suit {
                /// Invalid suit
                Invalid = 0,
                /// Spades
                Spades = 1,
                /// Clubs
                Clubs = 2,
                /// Hearts
                Hearts = 3,
                /// Diamonds
                Diamonds = 4,
                /// Joker
                Joker = 5,
                /// Other
                Other = 6,
            }

            /// Card's rank
            #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
            #[repr(i32)]
            pub enum Rank {
                /// Invalid rank
                Invalid = 0,
                Two = 2,
                Three = 3,
                Four = 4,
                Five = 5,
                Six = 6,
                Seven = 7,
                Eight = 8,
                Nine = 9,
                Ten = 10,
                Jack = 11,
                Queen = 12,
                King = 13,
                Ace = 14,
                Joker = 15,
                Other = 16,
            }
        }

        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum HandType {
            Dealt = 0,
            Final = 1,
        }
    }

    /// Section 7: Long polls
    /// - 7.13 Send Physical Reel Stop Information
    ///     - Poll 0x8F
    ///
    /// Query information about the reels' physical stops.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetPhysicalReelStopInformationRequest {}

    /// Section 7: Long polls
    /// - 7.13 Send Physical Reel Stop Information
    ///     - Table 7.13: Poll 0x8F
    ///
    /// Query information about the reels' physical stops.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetPhysicalReelStopInformationResponse {
        /// Physical stops for the reels, left-most reel first. (MaxCount:16)
        #[prost(uint32, repeated, tag = "1")]
        pub reel_stops: ::std::vec::Vec<u32>,
    }

    /// Section 7: Long polls
    /// - 7.14 Send Enabled Features
    ///     - Table 7.14a: Poll 0xA0
    ///
    /// Query information about the EGM's features.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnabledFeaturesRequest {
        /// Game number to query, 0 for EGM-wide.
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// Section 7: Long polls
    /// - 7.14 Send Enabled Features
    ///     - Table 7.14b: Poll 0xA0
    ///
    /// Query information about the EGM's features.
    ///
    /// Fields in this message map to the Feature Code bytes in table 7.14c to 7.14e as follows:
    /// - Table 7.14c Feature Codes 1 maps to:
    ///     - jackpot_multiplier_enabled
    ///     - aft_bonus_awards_enabled
    ///     - tournament_enabled
    ///     - validation_extensions_supported
    ///     - validation_style
    ///     - ticket_redemption_enabled
    /// - Table 7.14d Feature Codes 2 maps to:
    ///     - meter_model
    ///     - tickets_to_drop_and_cancelled_credits
    ///     - extended_meters_supported
    ///     - component_authentication_supported
    ///     - aft_supported
    ///     - multidenom_extensions_supported
    /// - Table 7.14e Feature Codes 3 maps to:
    ///     - fast_poll_rate_supported
    ///     - multiple_progressive_win_reporting_supported
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnabledFeaturesResponse {
        /// Game number to query, 0 for EGM-wide.
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        #[prost(bool, tag = "2")]
        pub jackpot_multiplier_enabled: bool,
        #[prost(bool, tag = "3")]
        pub aft_bonus_awards_enabled: bool,
        #[prost(bool, tag = "35")]
        pub legacy_bonus_awards_enabled: bool,
        #[prost(bool, tag = "4")]
        pub tournament_enabled: bool,
        #[prost(bool, tag = "5")]
        pub validation_extensions_supported: bool,
        #[prost(enumeration = "get_enabled_features_response::ValidationStyle", tag = "6")]
        pub validation_style: i32,
        #[prost(bool, tag = "7")]
        pub ticket_redemption_enabled: bool,
        #[prost(enumeration = "get_enabled_features_response::MeterModel", tag = "8")]
        pub meter_model: i32,
        #[prost(bool, tag = "9")]
        pub tickets_to_drop_and_cancelled_credits: bool,
        #[prost(bool, tag = "10")]
        pub extended_meters_supported: bool,
        #[prost(bool, tag = "11")]
        pub component_authentication_supported: bool,
        #[prost(bool, tag = "12")]
        pub aft_supported: bool,
        #[prost(bool, tag = "13")]
        pub multidenom_extensions_supported: bool,
        /// Features #3, see Table 7.14e
        #[prost(bool, tag = "14")]
        pub fast_poll_rate_supported: bool,
        #[prost(bool, tag = "15")]
        pub multiple_progressive_win_reporting_supported: bool,
    }

    pub mod get_enabled_features_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum ValidationStyle {
            None = 0,
            System = 1,
            SecureEnhanced = 2,
        }

        /// Feature Codes #2, see Table 7.14d
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum MeterModel {
            NotSpecified = 0,
            MeteredWhenWon = 1,
            MeteredWhenPlayedOrPaid = 2,
        }
    }

    /// Section 7: Long polls
    /// - 7.15 Send SAS Version ID and Gaming Machine Serial Number
    ///     - Poll 0x54
    ///
    /// Query information about the EGM's SAS version and serial number
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetSasVersionIdAndEgmSerialNumberRequest {}

    /// Section 7: Long polls
    /// - 7.15 Send SAS Version ID and Gaming Machine Serial Number
    ///     - Table 7.15: Poll 0x54
    ///
    /// Query information about the EGM's SAS version and serial number
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetSasVersionIdAndEgmSerialNumberResponse {
        /// SAS version (MaxLength:3)
        #[prost(bytes, tag = "1")]
        pub sas_version: std::vec::Vec<u8>,
        /// EGM serial number (MaxLength:64)
        #[prost(bytes, tag = "2")]
        pub egm_serial_number: std::vec::Vec<u8>,
    }

    /// Section 7: Long polls
    /// - 7.16 Send Cash Out Limt
    ///     - Table 7.16a: Poll 0xA4
    ///
    /// Query the EGM's cashout limt, in account denom units, for a given game.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCashoutLimitRequest {
        /// Game number to query, 0 for EGM-wide.
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// Section 7: Long polls
    /// - 7.16 Send Cash Out Limt
    ///     - Table 7.16b: Poll 0xA4
    ///
    /// Query the EGM's cashout limit, in accounting denomination, for a given game.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCashoutLimitResponse {
        /// Game number to query, 0 for EGM-wide.
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Cashout limit, in accounting denomination.
        #[prost(uint64, tag = "2")]
        pub cashout_limit: u64,
    }

    /// Date in SAS format
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SasDate {
        /// Day
        #[prost(uint32, tag = "1")]
        pub day: u32,
        /// Month
        #[prost(uint32, tag = "2")]
        pub month: u32,
        /// Year
        #[prost(uint32, tag = "3")]
        pub year: u32,
    }

    /// Time in SAS format
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SasTime {
        /// Hours
        #[prost(uint32, tag = "1")]
        pub hour: u32,
        /// Minutes
        #[prost(uint32, tag = "2")]
        pub minute: u32,
        /// Seconds
        #[prost(uint32, tag = "3")]
        pub seconds: u32,
    }

    /// Date and time in SAS format
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SasDateTime {
        /// Date
        #[prost(message, optional, tag = "1")]
        pub date: ::std::option::Option<SasDate>,
        /// Time
        #[prost(message, optional, tag = "2")]
        pub time: ::std::option::Option<SasTime>,
    }

    /// Section 7: Long polls
    /// - 7.17 Receive Date and Time
    ///     - Table 7.17: Poll 0x7F
    ///
    /// Set the EGM's date and time.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetDateAndTimeRequest {
        /// Date and time
        #[prost(message, optional, tag = "1")]
        pub date_time: ::std::option::Option<SasDateTime>,
    }

    /// Section 7: Long polls
    /// - 7.18 Send Current Date and Time
    ///     - Poll 0x7E
    ///
    /// Get the EGM's date and time.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetDateAndTimeRequest {}

    /// Section 7: Long polls
    /// - 7.18 Send Current Date and Time
    ///     - Table 7.18: Poll 0x7E
    ///
    /// Get the EGM's date and time.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetDateAndTimeResponse {
        /// Date and time
        #[prost(message, optional, tag = "1")]
        pub date_time: ::std::option::Option<SasDateTime>,
    }

    /// Section 7: Long polls
    /// - 7.19 Send Current Hopper Status
    ///     - Poll 0x4F
    ///
    /// Get the status of the hopper.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetHopperStatusRequest {}

    /// Section 7: Long polls
    /// - 7.19 Send Current Hopper Status
    ///     - Table 7.19a: Poll 0x4F
    ///
    /// Get the status of the hopper.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetHopperStatusResponse {
        /// Status code
        #[prost(enumeration = "get_hopper_status_response::HopperStatus", tag = "1")]
        pub status: i32,
        /// Percentage full, or 0xFF if unable to detect
        #[prost(uint32, tag = "2")]
        pub percent_full: u32,
        /// Level in number of coins/tokens, or 0xFF if unable to detect
        #[prost(uint32, tag = "3")]
        pub hopper_level: u32,
    }

    pub mod get_hopper_status_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum HopperStatus {
            Ok = 0,
            FloodedOptics = 1,
            ReverseCoin = 2,
            CoinTooShort = 3,
            CoinJam = 4,
            Runaway = 5,
            OpticsDisconnected = 6,
            Empty = 7,
            Other = 255,
        }
    }

    /// Section 7: Long polls
    /// - 7.20 Enable/Disable Auto Rebet
    ///     - Table 7.20: Poll 0xAA
    ///
    /// Enables/disables the EGM's auto-rebet functionality.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetAutoRebetStateRequest {
        /// New state for rebet.
        #[prost(bool, tag = "1")]
        pub enable_auto_rebet: bool,
    }

    /// Section 7: Long polls
    /// - 7.22 Send Token Denomination
    ///     - Poll 0xB3
    ///
    /// Query the EGM's coin-acceptor/hopper denomination.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetTokenDenominationRequest {}

    /// Section 7: Long polls
    /// - 7.22 Send Token Denomination
    ///     - Table 7.22: Poll 0xB3
    ///
    /// Query the EGM's coin-acceptor/hopper denomination.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetTokenDenominationResponse {
        /// Denomination used by the EGM's coin-acceptor/hopper denomination.
        #[prost(enumeration = "Denomination", tag = "1")]
        pub denomination: i32,
    }

    /// Section 7: Long polls
    /// - 7.23 Send Extended Game N Information
    ///     - Table 7.23a: Poll 0xB5
    ///
    /// Query additional data about a specific game on the EGM.
    ///
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetExtendedGameInformationRequest {
        /// Game to query, or zero for machine-wide
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// Section 7: Long polls
    /// - 7.23 Send Extended Game N Information
    ///     - Table 7.23b: Poll 0xB5
    ///
    /// Query additional data about a specific game on the EGM.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetExtendedGameInformationResponse {
        /// Game being reported
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Max bet
        #[prost(uint32, tag = "2")]
        pub max_bet: u32,
        /// SAS progressive group
        #[prost(uint32, tag = "3")]
        pub progressive_group: u32,
        /// Enabled SAS progressive levels (MaxCount:32)
        #[prost(uint32, repeated, tag = "4")]
        pub enabled_levels: ::std::vec::Vec<u32>,
        /// Name of the game, in ASCII (Optional) (MaxSize:32)
        #[prost(bytes, tag = "5")]
        pub game_name: std::vec::Vec<u8>,
        /// Name of the paytable, in ASCII (Optional) (MaxSize:32)
        #[prost(bytes, tag = "6")]
        pub paytable_name: std::vec::Vec<u8>,
        /// Number of wager catergories supported by the game.
        #[prost(uint32, tag = "7")]
        pub wager_categories: u32,
    }

    /// Section 7: Long polls
    /// - 7.24.2 Send Wager Category Information
    ///     - Table 7.24a: Poll 0xB4
    ///
    /// Query coin-in meter for a game's wager category
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetWagerCategoryInformationRequest {
        /// Game to query
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Requested wager category within the game
        #[prost(uint32, tag = "2")]
        pub wager_category: u32,
    }

    /// Section 7: Long polls
    /// - 7.24.2 Send Wager Category Information
    ///     - Table 7.24b: Poll 0xB4
    ///
    /// Query coin-in meter for a game's wager category
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetWagerCategoryInformationResponse {
        /// Game being queried
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        /// Wager category within the game being reported
        #[prost(uint32, tag = "2")]
        pub wager_category: u32,
        /// Theoretical payback percentage for the wager category using 4 digits of precision with an implied decimal point, 1234 => 12.34%  9876 => 98.76%
        #[prost(uint32, tag = "3")]
        pub payback_percentage: u32,
        /// Coin-in meter value
        #[prost(uint64, tag = "4")]
        pub coin_in_meter_value: u64,
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.1 AFT Register Gaming Machine
    ///     - Table 8.1a: Poll 0x73
    ///
    /// Registration process a debit system to associate an EGM with a POS terminal.
    /// - EGM may not be registered without non-zero asset number has been configured by the operator.
    /// - Not to be used for in-house or bonus transactions, which should use a registration key of all zeros.
    /// - EGM may not perform debit transactions until properly registered and issued a non-zero POS ID.
    /// - A POS ID is never required for in-house transfers.
    ///
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AftRegisterEgmRequest {
        /// Registration action to perform
        #[prost(enumeration = "aft_register_egm_request::AftRegistrationCode", tag = "1")]
        pub registration_code: i32,
        /// EGM asset number or house ID
        #[prost(uint32, tag = "2")]
        pub asset_number: u32,
        /// Registration key (MaxSize:32)
        #[prost(bytes, tag = "3")]
        pub registration_key: std::vec::Vec<u8>,
        /// Point-of-sale terminal ID
        #[prost(uint32, tag = "4")]
        pub pos_id: u32,
    }

    pub mod aft_register_egm_request {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum AftRegistrationCode {
            Init = 0,
            RegisterEgm = 1,
            RequestOperatorAcknowledgement = 64,
            UnregisterEgm = 128,
            ReadCurrentRegistration = 255,
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.1 AFT Register Gaming Machine
    ///     - Table 8.1b: Poll 0x73
    ///
    /// Registration process a debit system to associate an EGM with a POS terminal.
    ///
    /// If EGM has not been configured with an asset number, the field will be zero.
    /// If EGM has not received a registration key or POS ID, the fields will be zero.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AftRegisterEgmResponse {
        /// Registration status
        #[prost(enumeration = "aft_register_egm_response::AftRegistrationStatus", tag = "1")]
        pub registration_status: i32,
        /// Asset number
        #[prost(uint32, tag = "2")]
        pub asset_number: u32,
        /// Registration number
        #[prost(bytes, tag = "3")]
        pub registration_key: std::vec::Vec<u8>,
        /// POS terminal ID, 0 = no POS ID.
        #[prost(uint32, tag = "4")]
        pub pos_id: u32,
    }

    pub mod aft_register_egm_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum AftRegistrationStatus {
            Ready = 0,
            Registered = 1,
            Pending = 64,
            FtRegistrationStatusNotRegistered = 128,
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.2 AFT Game Lock and Status Request
    ///     - Table 8.2a: Poll 0x74
    ///
    /// Interrogate the AFT availability and request a lock on the machine state.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetAftLockAndStatusRequest {
        /// Lock action being requested
        #[prost(enumeration = "get_aft_lock_and_status_request::AftLockCode", tag = "1")]
        pub lock_code: i32,
        /// Transfers on which to lock
        #[prost(message, optional, tag = "2")]
        pub available_transfers: ::std::option::Option<get_aft_lock_and_status_request::AvailableTransfers>,
        /// Lock expiration duration in milliseconds.
        #[prost(uint32, tag = "3")]
        pub lock_expiration_time_ms: u32,
    }

    pub mod get_aft_lock_and_status_request {
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AvailableTransfers {
            #[prost(bool, tag = "1")]
            pub transfer_to_egm_ok: bool,
            #[prost(bool, tag = "2")]
            pub transfer_from_egm_ok: bool,
            #[prost(bool, tag = "3")]
            pub transfer_to_printer_ok: bool,
            #[prost(bool, tag = "4")]
            pub bonus_award_to_egm_ok: bool,
        }

        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum AftLockCode {
            RequestLock = 0,
            RequestCancelLock = 128,
            RequestQueryLock = 255,
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.2 AFT Game Lock and Status Request
    ///     - Table 8.2b: Poll 0x74
    ///
    /// Interrogate the AFT availability and request a lock on the machine state.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetAftLockAndStatusResponse {
        #[prost(uint32, tag = "1")]
        pub asset_number: u32,
        /// Status of the lock
        #[prost(enumeration = "get_aft_lock_and_status_response::AftLockStatus", tag = "2")]
        pub lock_status: i32,
        ///  Available transfers
        #[prost(message, optional, tag = "3")]
        pub available_transfers: ::std::option::Option<get_aft_lock_and_status_response::AvailableTransfers>,
        /// Cashout status
        #[prost(message, optional, tag = "4")]
        pub host_cashout_status: ::std::option::Option<get_aft_lock_and_status_response::HostCashoutStatus>,
        /// AFT status
        #[prost(message, optional, tag = "5")]
        pub aft_status: ::std::option::Option<get_aft_lock_and_status_response::AftStatus>,
        /// Maximum number of transactions in the buffer.
        #[prost(uint32, tag = "6")]
        pub max_transactions_in_buffer: u32,
        /// Current cashable amount on the EGM
        #[prost(uint64, tag = "7")]
        pub current_cashable_amount: u64,
        /// Current restricted amount on the EGM
        #[prost(uint64, tag = "8")]
        pub current_restricted_amount: u64,
        /// Current non-restricted amount on the EGM
        #[prost(uint64, tag = "9")]
        pub current_nonrestricted_amount: u64,
        /// Maximum amount that can be transferred to the EGM credit meter
        #[prost(uint64, tag = "10")]
        pub egm_transfer_limit: u64,
        /// Expiration of restricted amount, if non-zero
        #[prost(message, optional, tag = "11")]
        pub restricted_expiration: ::std::option::Option<Expiration>,
        /// Current restricted pool-id, if non-zero
        #[prost(uint32, tag = "12")]
        pub restricted_pool_id: u32,
    }

    pub mod get_aft_lock_and_status_response {
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AvailableTransfers {
            #[prost(bool, tag = "1")]
            pub transfer_to_egm_ok: bool,
            #[prost(bool, tag = "2")]
            pub transfer_from_egm_ok: bool,
            #[prost(bool, tag = "3")]
            pub transfer_to_printer_ok: bool,
            #[prost(bool, tag = "4")]
            pub bonus_award_to_egm_ok: bool,
            #[prost(bool, tag = "5")]
            pub lock_after_transfer_ok: bool,
        }

        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct HostCashoutStatus {
            #[prost(bool, tag = "1")]
            pub cashout_to_host_controlled_by_host: bool,
            #[prost(bool, tag = "2")]
            pub cashout_to_host_enabled: bool,
            #[prost(bool, tag = "3")]
            pub host_cashout_mode_hard: bool,
        }

        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AftStatus {
            #[prost(bool, tag = "1")]
            pub printer_available_for_transaction_receipts: bool,
            #[prost(bool, tag = "2")]
            pub transfer_to_host_less_than_full_amount_allowed: bool,
            #[prost(bool, tag = "3")]
            pub custom_ticket_data_supported: bool,
            #[prost(bool, tag = "4")]
            pub aft_registered: bool,
            #[prost(bool, tag = "5")]
            pub in_house_transfers_enabled: bool,
            #[prost(bool, tag = "6")]
            pub bonus_transfers_enabled: bool,
            #[prost(bool, tag = "7")]
            pub debit_transfers_enabled: bool,
            #[prost(bool, tag = "8")]
            pub any_aft_enabled: bool,
        }

        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum AftLockStatus {
            GameLocked = 0,
            GameLockedPending = 64,
            GameNotLocked = 255,
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.3 AFT Transfer Funds
    ///     - Table 8.3a: Poll 0x72
    ///
    /// Transfer funds between host and EGM
    ///
    /// To initiate a new transfer:
    /// - transfer_code *must* be one of:
    ///     - TRANSFER_CODE_TRANSFER_FULL_AMOUNT_ONLY
    ///     - TRANSFER_CODE_TRANSFER_PARTIAL_AMOUNT_OK
    /// - transaction_index *must* be 0x00
    ///
    /// To query an existing transfer:
    /// - transfer_code *must* be one of:
    ///     - TRANSFER_CODE_INTERROGATION_FE
    ///     - TRANSFER_CODE_INTERROGATION_FF
    /// - transaction_index is interpretted as follows:
    ///     - 0x00 = Current transaction
    ///     - 0x01-0x7F = Absolute index
    ///     - 0x81-0xFF = Relative index
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AftTransferFundsRequest {
        #[prost(enumeration = "aft_transfer_funds_request::TransferCode", tag = "1")]
        pub transfer_code: i32,
        /// 0 = current, 0x01-0x7F = Absolute index, 0x81-0xFF = relative index
        #[prost(uint32, tag = "2")]
        pub transaction_index: u32,
        #[prost(enumeration = "AftTransferType", tag = "3")]
        pub transfer_type: i32,
        /// Cashable amount on the EGM
        #[prost(uint64, tag = "4")]
        pub cashable_amount: u64,
        /// Restricted amount on the EGM
        #[prost(uint64, tag = "5")]
        pub restricted_amount: u64,
        /// Non-restricted amount on the EGM
        #[prost(uint64, tag = "6")]
        pub nonrestricted_amount: u64,
        #[prost(message, optional, tag = "7")]
        pub transfer_flags: ::std::option::Option<aft_transfer_funds_request::TransferFlags>,
        #[prost(uint32, tag = "8")]
        pub asset_number: u32,
        /// (MaxCount:32)
        #[prost(bytes, tag = "9")]
        pub transaction_id: std::vec::Vec<u8>,
        #[prost(message, optional, tag = "10")]
        pub expiration: ::std::option::Option<Expiration>,
        #[prost(uint32, tag = "11")]
        pub pool_id: u32,
        /// (MaxCount:128)
        #[prost(bytes, tag = "12")]
        pub receipt_data: std::vec::Vec<u8>,
        ///
        #[prost(uint32, tag = "13")]
        pub lock_timeout_ms: u32,
    }

    pub mod aft_transfer_funds_request {
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TransferFlags {
            #[prost(bool, tag = "1")]
            pub enable_host_cashout_control: bool,
            #[prost(bool, tag = "2")]
            pub enable_host_cashout: bool,
            #[prost(bool, tag = "3")]
            pub hard_host_cashout_mode: bool,
            #[prost(bool, tag = "4")]
            pub request_cashout_from_egm: bool,
            #[prost(bool, tag = "5")]
            pub lock_after_transfer: bool,
            #[prost(bool, tag = "6")]
            pub use_custom_ticket_data: bool,
            #[prost(bool, tag = "7")]
            pub accept_transfer_only_if_locked: bool,
            #[prost(bool, tag = "8")]
            pub transaction_receipt_request: bool,
        }

        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum TransferCode {
            TransferFullAmountOnly = 0,
            TransferPartialAmountOk = 1,
            CancelTransfer = 128,
            InterrogationFe = 254,
            InterrogationFf = 255,
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.3 AFT Transfer Funds
    ///     - Table 8.3c: Poll 0x72
    ///
    /// Transfer funds between host and EGM
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct AftTransferFundsResponse {
        #[prost(uint32, tag = "1")]
        pub transaction_buffer_position: u32,
        #[prost(enumeration = "AftTransferStatusCode", tag = "2")]
        pub transfer_status: i32,
        #[prost(enumeration = "AftTransactionReceiptStatus", tag = "3")]
        pub receipt_status: i32,
        #[prost(enumeration = "AftTransferType", tag = "4")]
        pub transfer_type: i32,
        #[prost(uint64, tag = "5")]
        pub cashable_amount: u64,
        #[prost(uint64, tag = "6")]
        pub restricted_amount: u64,
        #[prost(uint64, tag = "7")]
        pub nonrestricted_amount: u64,
        #[prost(uint32, tag = "8")]
        pub transfer_flags_bitmap: u32,
        #[prost(fixed32, tag = "9")]
        pub asset_number: u32,
        #[prost(bytes, tag = "10")]
        pub transaction_id: std::vec::Vec<u8>,
        #[prost(message, optional, tag = "11")]
        pub date_time: ::std::option::Option<SasDateTime>,
        #[prost(message, optional, tag = "12")]
        pub expiration: ::std::option::Option<Expiration>,
        #[prost(uint32, tag = "13")]
        pub pool_id: u32,
        #[prost(uint64, tag = "14")]
        pub cumulative_cashable_meter: u64,
        #[prost(uint64, tag = "15")]
        pub cumulative_restricted_meter: u64,
        #[prost(uint64, tag = "16")]
        pub cumulative_nonrestricted_meter: u64,
    }

    /// Ticket expiration expressed as either a specific date, or a number of days.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct Expiration {
        #[prost(oneof = "expiration::Expiration", tags = "1, 2")]
        pub expiration: ::std::option::Option<expiration::Expiration>,
    }

    pub mod expiration {
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Expiration {
            /// Expiration date
            #[prost(message, tag = "1")]
            Date(super::SasDate),
            /// Number of days before expiration
            #[prost(uint32, tag = "2")]
            NumDays(u32),
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.11.1 Set AFT Receipt Data
    ///     - Table 8.11.1a: Poll 0x75
    ///
    /// Set the AFT receipt data printed on registration reports and transaction receipts.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetAftReceiptDataRequest {
        /// Receipt data fields (MaxCount:16)
        #[prost(message, repeated, tag = "1")]
        pub fields: ::std::vec::Vec<set_aft_receipt_data_request::AftTransactionReceiptField>,
    }

    pub mod set_aft_receipt_data_request {
        /// <FIeld ID, Data> tuple
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AftTransactionReceiptField {
            #[prost(enumeration = "aft_transaction_receipt_field::AftTransactionReceiptFieldId", tag = "1")]
            pub id: i32,
            #[prost(bytes, tag = "2")]
            pub data: std::vec::Vec<u8>,
        }

        pub mod aft_transaction_receipt_field {
            #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
            #[repr(i32)]
            pub enum AftTransactionReceiptFieldId {
                Location = 0,
                Address1 = 1,
                Address2 = 2,
                InhouseLine1 = 16,
                InhouseLine2 = 17,
                InhouseLine3 = 18,
                InhouseLine4 = 19,
                DebitLine1 = 32,
                DebitLine2 = 33,
                DebitLine3 = 34,
                DebitLine4 = 35,
            }
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.12 Set AFT Ticket Data
    ///     - Table 8.12a: Poll 0x76
    ///
    /// Set the text/graphics printed during AFT transfers to tickets
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetCustomAftTicketDataRequest {
        #[prost(bool, tag = "1")]
        pub clear_all_fields: bool,
        #[prost(message, repeated, tag = "2")]
        pub field_definitions: ::std::vec::Vec<set_custom_aft_ticket_data_request::AftTicketFieldDefinition>,
    }

    pub mod set_custom_aft_ticket_data_request {
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct AftTicketFieldDefinition {
            #[prost(enumeration = "super::AftTicketFieldId", tag = "1")]
            pub field: i32,
            #[prost(bytes, tag = "2")]
            pub data: std::vec::Vec<u8>,
        }
    }

    /// Section 8: Advanced Funds Transfer
    /// - 8.12 Set AFT Ticket Data
    ///     - Table 8.12a: Poll 0x76
    ///
    /// Set the text/graphics printed during AFT transfers to tickets
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetCustomAftTicketDataResponse {
        /// ID's for each defined field.
        #[prost(enumeration = "AftTicketFieldId", repeated, tag = "1")]
        pub fields: ::std::vec::Vec<i32>,
    }
// ============================================================================
// Section 10: Progressives
// ============================================================================

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ProgressiveLevel {
        #[prost(fixed32, tag = "1")]
        pub level: u32,
        #[prost(uint64, tag = "2")]
        pub amount: u64,
    }

    /// 10.1 poll 0x86
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ProgressiveLevelBroadcastRequest {
        #[prost(fixed32, tag = "1")]
        pub group: u32,
        #[prost(message, repeated, tag = "2")]
        pub progressive_levels: ::std::vec::Vec<ProgressiveLevel>,
    }

    /// 10.4 poll 0x84
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetProgressiveWinAmountRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetProgressiveWinAmountResponse {
        #[prost(fixed32, tag = "1")]
        pub group: u32,
        #[prost(message, optional, tag = "2")]
        pub progressive_level: ::std::option::Option<ProgressiveLevel>,
    }

    /// 10.4.3 poll 0x87
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetProgressiveWinAmountsRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetProgressiveWinAmountsResponse {
        #[prost(fixed32, tag = "1")]
        pub group: u32,
        /// Max 32 levels
        #[prost(message, repeated, tag = "2")]
        pub progressive_levels: ::std::vec::Vec<ProgressiveLevel>,
    }

    /// 10.6 poll 0x83
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCumulativeProgressiveWinsRequest {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCumulativeProgressiveWinsResponse {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        #[prost(uint64, tag = "2")]
        pub cumulative_progressive_wins: u64,
    }
// ============================================================================
// Section 11: Tournament
// ============================================================================

    /// Poll 0x8C
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct EnterTournamentModeRequest {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        #[prost(message, optional, tag = "2")]
        pub tournament_time: ::std::option::Option<enter_tournament_mode_request::TournamentTime>,
        #[prost(uint32, tag = "3")]
        pub credits: u32,
        #[prost(bool, tag = "4")]
        pub tournament_pulses_enabled: bool,
    }

    pub mod enter_tournament_mode_request {
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct TournamentTime {
            #[prost(uint32, tag = "1")]
            pub minutes: u32,
            #[prost(uint32, tag = "2")]
            pub seconds: u32,
        }
    }

    /// Poll 0x8C (with time and credits set to zero.)
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ExitTournamentModeRequest {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    /// poll 0x0E
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetRealTimeEventReportingRequest {
        #[prost(bool, tag = "1")]
        pub enable_realtime_event_reporting: bool,
    }

    /// Sent by EGM as a response to a long-poll instead of the xxxResponse message.
    /// Host treats the RTE response as a NAK to the poll, and will resend it.
    /// poll 0xFF
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ExceptionMessage {
        #[prost(enumeration = "ExceptionCode", tag = "1")]
        pub exception_code: i32,
        /// If exception_code is EXCEPTION_CODE_REAL_TIME_EVENT, the event_message field holds the exception details.
        #[prost(oneof = "exception_message::RealTimeEvent", tags = "2, 3, 4, 5, 6, 7, 8, 9")]
        pub real_time_event: ::std::option::Option<exception_message::RealTimeEvent>,
    }

    pub mod exception_message {
        /// If exception_code is EXCEPTION_CODE_REAL_TIME_EVENT, the event_message field holds the exception details.
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum RealTimeEvent {
            #[prost(message, tag = "2")]
            BillAcceptedEventMessage(super::BillAcceptedEventMessage),
            #[prost(message, tag = "3")]
            LegacyBonusPayAwardedEventMessage(super::LegacyBonusPayAwardedEventMessage),
            #[prost(message, tag = "4")]
            GameStartEventMessage(super::GameStartEventMessage),
            #[prost(message, tag = "5")]
            GameEndEventMessage(super::GameEndEventMessage),
            #[prost(message, tag = "6")]
            ReelStoppedEventMessage(super::ReelStoppedEventMessage),
            #[prost(message, tag = "7")]
            GameRecallEnteredEventMessage(super::GameRecallEnteredEventMessage),
            #[prost(message, tag = "8")]
            CardHeldStateEventMessage(super::CardHeldStateEventMessage),
            #[prost(message, tag = "9")]
            GameSelectedEventMessage(super::GameSelectedEventMessage),
        }
    }

    /// Exception code 0x4F
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BillAcceptedEventMessage {
        #[prost(enumeration = "BillAcceptorCountryCode", tag = "1")]
        pub country_code: i32,
        #[prost(enumeration = "BillDenominationFlag", tag = "2")]
        pub denomination: i32,
        #[prost(uint32, tag = "3")]
        pub num_bills_accepted: u32,
    }

    /// Exception code 0x7C
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct LegacyBonusPayAwardedEventMessage {
        #[prost(bool, tag = "1")]
        pub is_deductible: bool,
        #[prost(uint32, tag = "2")]
        pub multiplier: u32,
        #[prost(uint64, tag = "3")]
        pub multiplied_win: u64,
        #[prost(enumeration = "TaxStatus", tag = "4")]
        pub tax_status: i32,
        #[prost(uint64, tag = "5")]
        pub bonus: u64,
    }

    /// Exception code 0x7E
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GameStartEventMessage {
        #[prost(uint64, tag = "1")]
        pub credits_wagered: u64,
        #[prost(uint64, tag = "2")]
        pub total_coin_in: u64,
        #[prost(message, optional, tag = "3")]
        pub wager_type: ::std::option::Option<game_start_event_message::WagerType>,
        #[prost(uint32, tag = "4")]
        pub progressive_group: u32,
    }

    pub mod game_start_event_message {
        #[derive(Clone, PartialEq, ::prost::Message)]
        pub struct WagerType {
            #[prost(enumeration = "super::Denomination", tag = "1")]
            pub denomination: i32,
            #[prost(bool, tag = "2")]
            pub is_multi_denomination: bool,
            #[prost(bool, tag = "3")]
            pub max_bet_wagered: bool,
        }
    }

    /// Exception code 0x7F
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GameEndEventMessage {
        #[prost(uint64, tag = "1")]
        pub game_win: u64,
    }

    /// Exception code 0x88
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ReelStoppedEventMessage {
        #[prost(uint32, tag = "1")]
        pub reel_number: u32,
        #[prost(uint32, tag = "2")]
        pub physical_stop: u32,
    }

    /// Exception code 0x8A
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GameRecallEnteredEventMessage {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        #[prost(uint32, tag = "2")]
        pub recall_entry_index: u32,
    }

    /// Exception code 0x8B
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct CardHeldStateEventMessage {
        #[prost(uint32, tag = "1")]
        pub card_number: u32,
        #[prost(bool, tag = "2")]
        pub card_held: bool,
    }

    /// Exception code 0x8C
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GameSelectedEventMessage {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }
// ============================================================================
// Section 13: Bonusing
// ============================================================================

    /// poll 0x8A
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct InitiateLegacyBonusRequest {
        #[prost(uint64, tag = "1")]
        pub bonus_amount: u64,
        #[prost(enumeration = "TaxStatus", tag = "2")]
        pub tax_status: i32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BonusMultiplier {
        #[prost(uint32, tag = "1")]
        pub multiplier: u32,
        #[prost(bool, tag = "2")]
        pub is_deductible: bool,
    }

    /// poll 0x8B
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MultipliedJackpotRequest {
        #[prost(uint64, tag = "1")]
        pub minimum_win: u64,
        #[prost(uint64, tag = "2")]
        pub maximum_win: u64,
        #[prost(message, optional, tag = "3")]
        pub bonus_multiplier: ::std::option::Option<BonusMultiplier>,
        #[prost(bool, tag = "4")]
        pub enable_multiplied_jackpots: bool,
        #[prost(bool, tag = "5")]
        pub limit_to_max_bet_wagers: bool,
    }

    /// poll 0x90
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetLegacyBonusWinAmountRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetLegacyBonusWinAmountResponse {
        #[prost(message, optional, tag = "1")]
        pub bonus_multiplier: ::std::option::Option<BonusMultiplier>,
        #[prost(uint64, tag = "2")]
        pub multiplied_win: u64,
        #[prost(enumeration = "TaxStatus", tag = "3")]
        pub tax_status: i32,
        #[prost(uint64, tag = "4")]
        pub bonus_amount: u64,
    }

    /// poll 0x9A
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetLegacyBonusMetersRequest {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetLegacyBonusMetersResponse {
        #[prost(uint32, tag = "1")]
        pub game_number: u32,
        #[prost(uint64, tag = "2")]
        pub deductible_bonus_meter: u64,
        #[prost(uint64, tag = "3")]
        pub nondeductible_bonus_meter: u64,
        #[prost(uint64, tag = "4")]
        pub wager_match: u64,
    }

    /// poll 0x2E
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GameDelayRequest {
        #[prost(uint32, tag = "1")]
        pub delay_ms: u32,
    }
// ============================================================================
// Section 14: Jackpot Handpay Reset
// ============================================================================

    /// poll 0xA8
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct EnableJackpotHandpayResetRequest {
        #[prost(enumeration = "enable_jackpot_handpay_reset_request::HandpayResetMethod", tag = "1")]
        pub handpay_reset_method: i32,
    }

    pub mod enable_jackpot_handpay_reset_request {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum HandpayResetMethod {
            StandardHandpay = 0,
            ResetToCreditMeter = 1,
        }
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct EnableJackpotHandpayResetResponse {
        #[prost(enumeration = "enable_jackpot_handpay_reset_response::HandpayResetResponse", tag = "1")]
        pub response: i32,
    }

    pub mod enable_jackpot_handpay_reset_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum HandpayResetResponse {
            ResetMethodEnabled = 0,
            CannotEnableResetMethod = 1,
            NotInHandpay = 2,
        }
    }
// ============================================================================
// Section 15: Validation and Ticket Redemption
// ============================================================================

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ValidationControl {
        #[prost(bool, tag = "1")]
        pub printer_as_cashout_device: bool,
        #[prost(bool, tag = "2")]
        pub printer_as_handpay_receipt_device: bool,
        #[prost(bool, tag = "3")]
        pub validate_handpays_and_receipts: bool,
        #[prost(bool, tag = "4")]
        pub print_restricted_tickets: bool,
        #[prost(bool, tag = "5")]
        pub tickets_for_foreign_restricted_amounts: bool,
        #[prost(bool, tag = "6")]
        pub ticket_redemption: bool,
        #[prost(bool, tag = "7")]
        pub secure_enhanced_validation_configuration: bool,
    }

    /// poll 0x7B
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetExtendedValidationStatusRequest {
        #[prost(message, optional, tag = "1")]
        pub control_mask: ::std::option::Option<ValidationControl>,
        #[prost(message, optional, tag = "2")]
        pub status_control: ::std::option::Option<ValidationControl>,
        #[prost(uint32, tag = "3")]
        pub cashable_expiration_days: u32,
        #[prost(uint32, tag = "4")]
        pub default_restricted_expiration_days: u32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetExtendedValidationStatusResponse {
        #[prost(uint32, tag = "1")]
        pub asset_number: u32,
        #[prost(message, optional, tag = "2")]
        pub status: ::std::option::Option<ValidationControl>,
        #[prost(uint32, tag = "3")]
        pub cashable_expiration_days: u32,
        #[prost(uint32, tag = "4")]
        pub default_restricted_expiration_days: u32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct ExtendedTicketFieldDefinition {
        #[prost(enumeration = "ExtendedTicketField", tag = "1")]
        pub field: i32,
        #[prost(bytes, tag = "2")]
        pub data: std::vec::Vec<u8>,
    }

    /// 15.3 Set Extended Ticket Data
    /// poll 0x7C
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetExtendedTicketDataRequest {
        /// Max 8
        #[prost(message, repeated, tag = "1")]
        pub ticket_field_definitions: ::std::vec::Vec<ExtendedTicketFieldDefinition>,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetExtendedTicketDataResponse {
        #[prost(bool, tag = "1")]
        pub ticket_data_status_valid: bool,
    }

    /// 15.4 poll 0x7D
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetTicketDataRequest {
        #[prost(uint32, tag = "1")]
        pub host_id: u32,
        #[prost(uint32, tag = "2")]
        pub expiration_days: u32,
        #[prost(bytes, tag = "3")]
        pub location: std::vec::Vec<u8>,
        #[prost(bytes, tag = "4")]
        pub address_1: std::vec::Vec<u8>,
        #[prost(bytes, tag = "5")]
        pub address_2: std::vec::Vec<u8>,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetTicketDataResponse {
        #[prost(bool, tag = "1")]
        pub ticket_data_status_valid: bool,
    }

    /// poll 0x3D
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCashoutTicketInformationRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCashoutTicketInformationResponse {
        #[prost(fixed32, tag = "1")]
        pub validation_number: u32,
        #[prost(uint64, tag = "2")]
        pub ticket_amount: u64,
    }

    /// poll 0x4C
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetSecureEnhancedValidationIdRequest {
        #[prost(uint32, tag = "1")]
        pub machine_id: u32,
        #[prost(uint32, tag = "2")]
        pub sequence_number: u32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetSecureEnhancedValidationIdResponse {
        #[prost(uint32, tag = "1")]
        pub machine_id: u32,
        #[prost(uint32, tag = "2")]
        pub sequence_number: u32,
    }

    /// poll 0x57
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetPendingCashoutInformationRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetPendingCashoutInformationResponse {
        #[prost(enumeration = "get_pending_cashout_information_response::CashoutType", tag = "1")]
        pub cashout_type: i32,
        #[prost(uint64, tag = "2")]
        pub amount: u64,
    }

    pub mod get_pending_cashout_information_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum CashoutType {
            CashableTicket = 0,
            RestrictedPromotionalTicket = 1,
            NotWaitingForSystemValidation = 128,
        }
    }

    /// poll 0x58
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetValidationNumberRequest {
        #[prost(uint32, tag = "1")]
        pub validation_system_id: u32,
        #[prost(uint64, tag = "2")]
        pub validation_number: u64,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SetValidationNumberResponse {
        #[prost(enumeration = "set_validation_number_response::SetValidationNumberStatus", tag = "1")]
        pub status: i32,
    }

    pub mod set_validation_number_response {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum SetValidationNumberStatus {
            Ack = 0,
            NotInCashout = 128,
            Rejected = 129,
        }
    }

    /// poll 0x4D
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnhancedValidationInformationRequest {
        #[prost(enumeration = "get_enhanced_validation_information_request::QueryMode", tag = "1")]
        pub query_mode: i32,
        #[prost(uint32, tag = "2")]
        pub index: u32,
    }

    pub mod get_enhanced_validation_information_request {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum QueryMode {
            ReadAndRemove = 0,
            ReadOnly = 1,
        }
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnhancedValidationInformationResponse {
        #[prost(enumeration = "ValidationType", tag = "1")]
        pub validation_type: i32,
        #[prost(uint32, tag = "2")]
        pub index: u32,
        #[prost(message, optional, tag = "3")]
        pub date_time: ::std::option::Option<SasDateTime>,
        #[prost(uint64, tag = "4")]
        pub validation_number: u64,
        #[prost(uint64, tag = "5")]
        pub amount: u64,
        #[prost(uint32, tag = "6")]
        pub ticket_number: u32,
        #[prost(uint32, tag = "7")]
        pub system_id: u32,
        #[prost(message, optional, tag = "8")]
        pub expiration: ::std::option::Option<Expiration>,
        #[prost(uint32, tag = "9")]
        pub restricted_pool_id: u32,
    }
// ----------------------------------------------------------------------------
// 15.11
// ----------------------------------------------------------------------------
// poll 0x70
// When ticket inserted, EGM sends Exception 0x67.
// Host sends 0x70 to query the ticket validation data

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetTicketValidationDataRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetTicketValidationDataResponse {
        #[prost(bool, tag = "1")]
        pub ticket_in_escrow: bool,
        #[prost(uint64, tag = "2")]
        pub ticket_amount: u64,
        #[prost(uint32, tag = "3")]
        pub parsing_code: u32,
        /// 32-bytes max
        #[prost(bytes, tag = "4")]
        pub validation_data: std::vec::Vec<u8>,
    }
// poll 0x71

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct RedeemTicketRequest {
        #[prost(enumeration = "TicketTransferCode", tag = "1")]
        pub transfer_code: i32,
        #[prost(uint64, tag = "2")]
        pub amount: u64,
        #[prost(uint32, tag = "3")]
        pub parsing_code: u32,
        /// 32 bytes max
        #[prost(bytes, tag = "4")]
        pub validation_data: std::vec::Vec<u8>,
        #[prost(message, optional, tag = "5")]
        pub restricted_expiration: ::std::option::Option<Expiration>,
        #[prost(uint32, tag = "6")]
        pub restricted_pool_id: u32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct RedeemTicketResponse {
        #[prost(enumeration = "EgmTicketRedemptionStatus", tag = "1")]
        pub redemption_status: i32,
        #[prost(uint64, tag = "2")]
        pub amount: u64,
        #[prost(uint32, tag = "3")]
        pub parsing_code: u32,
        /// 32 bytes max
        #[prost(bytes, tag = "4")]
        pub validation_data: std::vec::Vec<u8>,
    }
// ----------------------------------------------------------------------------
// 15.13 Send Validation Meters
// ----------------------------------------------------------------------------

    /// Poll 0x50
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SendValidationMetersRequest {
        #[prost(enumeration = "ValidationType", tag = "1")]
        pub validation_type: i32,
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SendValidationMetersResponse {
        #[prost(enumeration = "ValidationType", tag = "1")]
        pub validation_type: i32,
        #[prost(uint32, tag = "2")]
        pub count: u32,
        #[prost(uint64, tag = "3")]
        pub total_amount: u64,
    }

    /// poll 0xB0
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MultidenomPreambleRequest {
        #[prost(enumeration = "Denomination", tag = "1")]
        pub denomination: i32,
        /// Inner request
        #[prost(oneof = "multidenom_preamble_request::InnerRequest", tags = "2, 3, 4, 5")]
        pub inner_request: ::std::option::Option<multidenom_preamble_request::InnerRequest>,
    }

    pub mod multidenom_preamble_request {
        /// Inner request
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum InnerRequest {
            #[prost(message, tag = "2")]
            SetGameEnableStateRequest(super::SetGameEnableStateRequest),
            #[prost(message, tag = "3")]
            GetMeterValuesRequest(super::GetMeterValuesRequest),
            #[prost(message, tag = "4")]
            GetEnabledGameNumbersRequest(super::GetEnabledGameNumbersRequest),
            #[prost(message, tag = "5")]
            GetExtendedGameInformationRequest(super::GetExtendedGameInformationRequest),
        }
    }

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct MultidenomPreambleResponse {
        #[prost(enumeration = "Denomination", tag = "1")]
        pub denomination: i32,
        /// Indicates status of the outer request.
        #[prost(enumeration = "MultidenomPreambleError", tag = "2")]
        pub error_code: i32,
        /// Inner response
        #[prost(oneof = "multidenom_preamble_response::InnerResponse", tags = "3, 4, 6, 7, 8")]
        pub inner_response: ::std::option::Option<multidenom_preamble_response::InnerResponse>,
    }

    pub mod multidenom_preamble_response {
        /// Inner response
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum InnerResponse {
            /// Indicates the EGM NACK'ed the corresponding request.
            #[prost(message, tag = "3")]
            NackResponse(super::NackResponse),
            /// Indicates the EGM ACK'ed the corresponding request.
            #[prost(message, tag = "4")]
            AckResponse(super::AckResponse),
            #[prost(message, tag = "6")]
            GetMeterValuesResponse(super::GetMeterValuesResponse),
            #[prost(message, tag = "7")]
            GetEnabledGameNumbersResponse(super::GetEnabledGameNumbersResponse),
            #[prost(message, tag = "8")]
            GetExtendedGameInformationResponse(super::GetExtendedGameInformationResponse),
        }
    }
// 16.3

    /// poll 0xB1
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCurrentPlayerDenominationRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetCurrentPlayerDenominationResponse {
        #[prost(enumeration = "Denomination", tag = "1")]
        pub denomination: i32,
    }

    /// 16.4, poll 0xB2
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnabledPlayerDenominationsRequest {}

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetEnabledPlayerDenominationsResponse {
        #[prost(enumeration = "Denomination", repeated, tag = "1")]
        pub denominations: ::std::vec::Vec<i32>,
    }
// 17.1 Send Authentication Info, poll 0x6E

    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct GetAuthenticationInfoRequest {
        #[prost(enumeration = "get_authentication_info_request::AuthenticationAction", tag = "1")]
        pub action: i32,
        /// For actions that act upon a specific component, the target
        /// component is addressed either by index or name.
        #[prost(oneof = "get_authentication_info_request::ComponentAddress", tags = "2, 3")]
        pub component_address: ::std::option::Option<get_authentication_info_request::ComponentAddress>,
    }

    pub mod get_authentication_info_request {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
        #[repr(i32)]
        pub enum AuthenticationAction {
            QueryNumberOfInstalledComponents = 0,
            ReadComponentStatus = 1,
            AuthenticateComponent = 2,
            QueryAuthenticationStatus = 3,
        }

        /// For actions that act upon a specific component, the target
        /// component is addressed either by index or name.
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum ComponentAddress {
            #[prost(bytes, tag = "2")]
            ByName(std::vec::Vec<u8>),
            #[prost(uint32, tag = "3")]
            ByIndex(u32),
        }
    }

    /// =====================================================================================================================
    /// SAS Request
    /// =====================================================================================================================
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SasRequest {
        #[prost(uint32, tag = "1")]
        pub machine_id: u32,
        /// If no specific request indicated, treat the request as a general poll.
        #[prost(oneof = "sas_request::Request", tags = "2, 33, 10, 47, 8, 9, 45, 81, 83, 85, 24, 27, 148, 31, 72, 142, 143, 160, 84, 164, 127, 126, 79, 170, 179, 181, 180, 115, 116, 114, 117, 118, 134, 132, 135, 131, 140, 141, 14, 138, 139, 144, 154, 46, 168, 123, 124, 125, 61, 76, 87, 88, 77, 112, 113, 80, 176, 177, 178, 110")]
        pub request: ::std::option::Option<sas_request::Request>,
    }

    pub mod sas_request {
        /// If no specific request indicated, treat the request as a general poll.
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Request {
            /// Section 2.2.1: General poll.
            /// EGM will reply with an ExceptionMessage reply reporting its state (EXCEPTION_CODE_NO_ACTIVITY if no activity)
            #[prost(message, tag = "2")]
            GeneralPoll(super::GeneralPoll),
            /// Section 6: ROM Signature
            #[prost(message, tag = "33")]
            RomSignatureRequest(super::RomSignatureRequest),
            /// Section 7: Long polls
            #[prost(message, tag = "10")]
            ControlGamingMachineRequest(super::ControlGamingMachineRequest),
            #[prost(message, tag = "47")]
            GetMeterValuesRequest(super::GetMeterValuesRequest),
            #[prost(message, tag = "8")]
            ConfigureBillDenominationsRequest(super::ConfigureBillDenominationsRequest),
            #[prost(message, tag = "9")]
            SetGameEnableStateRequest(super::SetGameEnableStateRequest),
            #[prost(message, tag = "45")]
            GetTotalHandPaidCancelledCreditsRequest(super::GetTotalHandPaidCancelledCreditsRequest),
            #[prost(message, tag = "81")]
            GetNumberOfGamesImplementedRequest(super::GetNumberOfGamesImplementedRequest),
            #[prost(message, tag = "83")]
            GetGameConfigurationRequest(super::GetGameConfigurationRequest),
            #[prost(message, tag = "85")]
            GetSelectedGameNumberRequest(super::GetSelectedGameNumberRequest),
            #[prost(message, tag = "24")]
            GetEnabledGameNumbersRequest(super::GetEnabledGameNumbersRequest),
            #[prost(message, tag = "27")]
            GetHandpayInformationRequest(super::GetHandpayInformationRequest),
            #[prost(message, tag = "148")]
            RemoteHandpayResetRequest(super::RemoteHandpayResetRequest),
            #[prost(message, tag = "31")]
            GetMachineInfoRequest(super::GetMachineInfoRequest),
            #[prost(message, tag = "72")]
            GetLastAcceptedBillInfoRequest(super::GetLastAcceptedBillInfoRequest),
            #[prost(message, tag = "142")]
            GetCardInformationRequest(super::GetCardInformationRequest),
            #[prost(message, tag = "143")]
            GetPhysicalReelStopInformationRequest(super::GetPhysicalReelStopInformationRequest),
            #[prost(message, tag = "160")]
            GetEnabledFeaturesRequest(super::GetEnabledFeaturesRequest),
            #[prost(message, tag = "84")]
            GetSasVersionIdAndEgmSerialNumberRequest(super::GetSasVersionIdAndEgmSerialNumberRequest),
            #[prost(message, tag = "164")]
            GetCashoutLimitRequest(super::GetCashoutLimitRequest),
            #[prost(message, tag = "127")]
            SetDateAndTimeRequest(super::SetDateAndTimeRequest),
            #[prost(message, tag = "126")]
            GetDateAndTimeRequest(super::GetDateAndTimeRequest),
            #[prost(message, tag = "79")]
            GetHopperStatusRequest(super::GetHopperStatusRequest),
            #[prost(message, tag = "170")]
            SetAutoRebetStateRequest(super::SetAutoRebetStateRequest),
            #[prost(message, tag = "179")]
            GetTokenDenominationRequest(super::GetTokenDenominationRequest),
            #[prost(message, tag = "181")]
            GetExtendedGameInformationRequest(super::GetExtendedGameInformationRequest),
            #[prost(message, tag = "180")]
            GetWagerCategoryInformationRequest(super::GetWagerCategoryInformationRequest),
            /// Section 8: AFT
            #[prost(message, tag = "115")]
            AftRegisterEgmRequest(super::AftRegisterEgmRequest),
            #[prost(message, tag = "116")]
            GetAftLockAndStatusRequest(super::GetAftLockAndStatusRequest),
            #[prost(message, tag = "114")]
            AftTransferFundsRequest(super::AftTransferFundsRequest),
            #[prost(message, tag = "117")]
            SetAftReceiptDataRequest(super::SetAftReceiptDataRequest),
            #[prost(message, tag = "118")]
            SetCustomAftTicketDataRequest(super::SetCustomAftTicketDataRequest),
            /// Section 10: Progressives
            #[prost(message, tag = "134")]
            ProgressiveLevelBroadcastRequest(super::ProgressiveLevelBroadcastRequest),
            #[prost(message, tag = "132")]
            GetProgressiveWinAmountRequest(super::GetProgressiveWinAmountRequest),
            #[prost(message, tag = "135")]
            GetProgressiveWinAmountsRequest(super::GetProgressiveWinAmountsRequest),
            #[prost(message, tag = "131")]
            GetCumulativeProgressiveWinsRequest(super::GetCumulativeProgressiveWinsRequest),
            /// Section 11: Tournament
            #[prost(message, tag = "140")]
            EnterTournamentModeRequest(super::EnterTournamentModeRequest),
            #[prost(message, tag = "141")]
            ExitTournamentModeRequest(super::ExitTournamentModeRequest),
            /// Section 12: Real-time Events
            #[prost(message, tag = "14")]
            SetRealTimeEventReportingRequest(super::SetRealTimeEventReportingRequest),
            /// Section 13: Bonusing
            #[prost(message, tag = "138")]
            InitiateLegacyBonusRequest(super::InitiateLegacyBonusRequest),
            #[prost(message, tag = "139")]
            MultipliedJackpotRequest(super::MultipliedJackpotRequest),
            #[prost(message, tag = "144")]
            GetLegacyBonusWinAmountRequest(super::GetLegacyBonusWinAmountRequest),
            #[prost(message, tag = "154")]
            GetLegacyBonusMetersRequest(super::GetLegacyBonusMetersRequest),
            #[prost(message, tag = "46")]
            GameDelayRequest(super::GameDelayRequest),
            /// Section 14: Jackpot Handpay Reset
            #[prost(message, tag = "168")]
            EnableJackpotHandpayResetRequest(super::EnableJackpotHandpayResetRequest),
            /// Section 15: Validation and Ticket Redemption
            #[prost(message, tag = "123")]
            SetExtendedValidationStatusRequest(super::SetExtendedValidationStatusRequest),
            #[prost(message, tag = "124")]
            SetExtendedTicketDataRequest(super::SetExtendedTicketDataRequest),
            #[prost(message, tag = "125")]
            SetTicketDataRequest(super::SetTicketDataRequest),
            #[prost(message, tag = "61")]
            GetCashoutTicketInformationRequest(super::GetCashoutTicketInformationRequest),
            #[prost(message, tag = "76")]
            SetSecureEnhancedValidationIdRequest(super::SetSecureEnhancedValidationIdRequest),
            #[prost(message, tag = "87")]
            GetPendingCashoutInformationRequest(super::GetPendingCashoutInformationRequest),
            #[prost(message, tag = "88")]
            SetValidationNumberRequest(super::SetValidationNumberRequest),
            #[prost(message, tag = "77")]
            GetEnhancedValidationInformationRequest(super::GetEnhancedValidationInformationRequest),
            #[prost(message, tag = "112")]
            GetTicketValidationDataRequest(super::GetTicketValidationDataRequest),
            #[prost(message, tag = "113")]
            RedeemTicketRequest(super::RedeemTicketRequest),
            #[prost(message, tag = "80")]
            SendValidationMetersRequest(super::SendValidationMetersRequest),
            /// Section 16: Multidenom extensions
            #[prost(message, tag = "176")]
            MultidenomPreambleRequest(super::MultidenomPreambleRequest),
            #[prost(message, tag = "177")]
            GetCurrentPlayerDenominationRequest(super::GetCurrentPlayerDenominationRequest),
            #[prost(message, tag = "178")]
            GetEnabledPlayerDenominationsRequest(super::GetEnabledPlayerDenominationsRequest),
            /// Section 17: Component Authentication
            #[prost(message, tag = "110")]
            GetAuthenticationInfoRequest(super::GetAuthenticationInfoRequest),
        }
    }

    ///*
    /// SAS Response
    ///
    /// This is the message the EGM sends in response to a specific request, or general poll.
    ///
    /// Every response will contain the *machine_id* for the EGM which is responding.
    ///
    /// The EGM's response data is dependent upon the EGM's state and the request which initiated the response.  It may:
    ///    - NACK any message by responding with a *NackResponse*
    ///    - Respond to a general poll with an *ExceptionMessage*
    ///    - Respond to a general poll with *RomSignatureResponse* when signature calculation is complete.
    ///    - Respond to any request with an event-ful *ExceptionMessage*,  if real-time event reporting is enabled
    ///    - Reply to a specific *xxxRequest* with the corresponding *xxxResponse*
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct SasResponse {
        /// SAS ID for the EGM responding.
        #[prost(uint32, tag = "1")]
        pub machine_id: u32,
        #[prost(oneof = "sas_response::Response", tags = "2, 3, 4, 33, 47, 45, 81, 83, 85, 24, 27, 148, 31, 72, 142, 143, 160, 84, 164, 126, 79, 179, 181, 180, 115, 116, 114, 118, 132, 135, 131, 144, 154, 168, 123, 124, 125, 61, 76, 87, 88, 77, 112, 113, 80, 176, 177, 178")]
        pub response: ::std::option::Option<sas_response::Response>,
    }

    pub mod sas_response {
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum Response {
            /// Indicates the EGM NACK'ed the corresponding request.
            #[prost(message, tag = "2")]
            NackResponse(super::NackResponse),
            /// Indicates the EGM ACK'ed the corresponding request.
            #[prost(message, tag = "3")]
            AckResponse(super::AckResponse),
            /// Exception reporting EGM event.
            #[prost(message, tag = "4")]
            Exception(super::ExceptionMessage),
            /// Response to a general poll when signature calculation is complete. (Section 6: ROM Signature)
            #[prost(message, tag = "33")]
            RomSignatureResponse(super::RomSignatureResponse),
            /// Section 7: Long polls
            #[prost(message, tag = "47")]
            GetMeterValuesResponse(super::GetMeterValuesResponse),
            #[prost(message, tag = "45")]
            GetTotalHandPaidCancelledCreditsResponse(super::GetTotalHandPaidCancelledCreditsResponse),
            #[prost(message, tag = "81")]
            GetNumberOfGamesImplementedResponse(super::GetNumberOfGamesImplementedResponse),
            #[prost(message, tag = "83")]
            GetGameConfigurationResponse(super::GetGameConfigurationResponse),
            #[prost(message, tag = "85")]
            GetSelectedGameNumberResponse(super::GetSelectedGameNumberResponse),
            #[prost(message, tag = "24")]
            GetEnabledGameNumbersResponse(super::GetEnabledGameNumbersResponse),
            #[prost(message, tag = "27")]
            GetHandpayInformationResponse(super::GetHandpayInformationResponse),
            #[prost(message, tag = "148")]
            RemoteHandpayResetResponse(super::RemoteHandpayResetResponse),
            #[prost(message, tag = "31")]
            GetMachineInfoResponse(super::GetMachineInfoResponse),
            #[prost(message, tag = "72")]
            GetLastAcceptedBillInfoResponse(super::GetLastAcceptedBillInfoResponse),
            #[prost(message, tag = "142")]
            GetCardInformationResponse(super::GetCardInformationResponse),
            #[prost(message, tag = "143")]
            GetPhysicalReelStopInformationResponse(super::GetPhysicalReelStopInformationResponse),
            #[prost(message, tag = "160")]
            GetEnabledFeaturesResponse(super::GetEnabledFeaturesResponse),
            #[prost(message, tag = "84")]
            GetSasVersionIdAndEgmSerialNumberResponse(super::GetSasVersionIdAndEgmSerialNumberResponse),
            #[prost(message, tag = "164")]
            GetCashoutLimitResponse(super::GetCashoutLimitResponse),
            #[prost(message, tag = "126")]
            GetDateAndTimeResponse(super::GetDateAndTimeResponse),
            #[prost(message, tag = "79")]
            GetHopperStatusResponse(super::GetHopperStatusResponse),
            #[prost(message, tag = "179")]
            GetTokenDenominationResponse(super::GetTokenDenominationResponse),
            #[prost(message, tag = "181")]
            GetExtendedGameInformationResponse(super::GetExtendedGameInformationResponse),
            #[prost(message, tag = "180")]
            GetWagerCategoryInformationResponse(super::GetWagerCategoryInformationResponse),
            /// Section 8: AFT
            #[prost(message, tag = "115")]
            AftRegisterEgmResponse(super::AftRegisterEgmResponse),
            #[prost(message, tag = "116")]
            GetAftLockAndStatusResponse(super::GetAftLockAndStatusResponse),
            #[prost(message, tag = "114")]
            AftTransferFundsResponse(super::AftTransferFundsResponse),
            #[prost(message, tag = "118")]
            SetCustomAftTicketDataResponse(super::SetCustomAftTicketDataResponse),
            /// Section 10: Progressives
            #[prost(message, tag = "132")]
            GetProgressiveWinAmountResponse(super::GetProgressiveWinAmountResponse),
            #[prost(message, tag = "135")]
            GetProgressiveWinAmountsResponse(super::GetProgressiveWinAmountsResponse),
            #[prost(message, tag = "131")]
            GetCumulativeProgressiveWinsResponse(super::GetCumulativeProgressiveWinsResponse),
            /// Section 13: Bonusing
            #[prost(message, tag = "144")]
            GetLegacyBonusWinAmountResponse(super::GetLegacyBonusWinAmountResponse),
            #[prost(message, tag = "154")]
            GetLegacyBonusMetersResponse(super::GetLegacyBonusMetersResponse),
            /// Section 14: Jackpot Handpay Reset
            #[prost(message, tag = "168")]
            EnableJackpotHandpayResetResponse(super::EnableJackpotHandpayResetResponse),
            /// Section 15: Validation and Ticket Redemption
            #[prost(message, tag = "123")]
            SetExtendedValidationStatusResponse(super::SetExtendedValidationStatusResponse),
            #[prost(message, tag = "124")]
            SetExtendedTicketDataResponse(super::SetExtendedTicketDataResponse),
            #[prost(message, tag = "125")]
            SetTicketDataResponse(super::SetTicketDataResponse),
            #[prost(message, tag = "61")]
            GetCashoutTicketInformationResponse(super::GetCashoutTicketInformationResponse),
            #[prost(message, tag = "76")]
            SetSecureEnhancedValidationIdResponse(super::SetSecureEnhancedValidationIdResponse),
            #[prost(message, tag = "87")]
            GetPendingCashoutInformationResponse(super::GetPendingCashoutInformationResponse),
            #[prost(message, tag = "88")]
            SetValidationNumberResponse(super::SetValidationNumberResponse),
            #[prost(message, tag = "77")]
            GetEnhancedValidationInformationResponse(super::GetEnhancedValidationInformationResponse),
            #[prost(message, tag = "112")]
            GetTicketValidationDataResponse(super::GetTicketValidationDataResponse),
            #[prost(message, tag = "113")]
            RedeemTicketResponse(super::RedeemTicketResponse),
            #[prost(message, tag = "80")]
            SendValidationMetersResponse(super::SendValidationMetersResponse),
            /// Section 16: Multidenom extensions
            #[prost(message, tag = "176")]
            MultidenomPreambleResponse(super::MultidenomPreambleResponse),
            #[prost(message, tag = "177")]
            GetCurrentPlayerDenominationResponse(super::GetCurrentPlayerDenominationResponse),
            #[prost(message, tag = "178")]
            GetEnabledPlayerDenominationsResponse(super::GetEnabledPlayerDenominationsResponse),
        }
    }

    /// Table B-1: SAS Long Poll Command Codes
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum LongPoll {
        Unknown = 0,
        Shutdown = 1,
        Startup = 2,
        SoundOff = 3,
        SoundOn = 4,
        ReelSpinOrGamePlaySoundsDisabled = 5,
        EnableBillAcceptor = 6,
        DisableBillAcceptor = 7,
        ConfigureBillDenominations = 8,
        EnableDisableGameN = 9,
        EnterMaintenanceMode = 10,
        ExitMaintenanceMode = 11,
        EnableDisableRealTimeEventReporting = 14,
        SendMeters10Through15 = 15,
        SendTotalCanceledCreditsMeter = 16,
        SendTotalCoinInMeter = 17,
        SendTotalCoinOutMeter = 18,
        SendTotalDropMeter = 19,
        SendTotalJackpotMeter = 20,
        SendGamesPlayedMeter = 21,
        SendGamesWonMeter = 22,
        SendGamesLostMeter = 23,
        SendGamesSincePowerUpDoorClosure = 24,
        SendMeters11Through15 = 25,
        SendCurrentCredits = 26,
        SendHandpayInformation = 27,
        SendMeters = 28,
        SendTotalBillMeters = 30,
        SendGamingMachineIdAndInformation = 31,
        SendTotalDollarValueOfBillsMeter = 32,
        RomSignatureVerification = 33,
        SendTrueCoinIn = 42,
        SendTrueCoinOut = 43,
        SendCurrentHopperLevel = 44,
        SendTotalHandPaidCancelledCredits = 45,
        DelayGame = 46,
        SendSelectedMetersForGameN = 47,
        SendDollar1BillsInMeter = 49,
        SendDollar2BillsInMeter = 50,
        SendDollar5BillsInMeter = 51,
        SendDollar10BillsInMeter = 52,
        SendDollar20BillsInMeter = 53,
        SendDollar50BillsInMeter = 54,
        SendDollar100BillsInMeter = 55,
        SendDollar500BillsInMeter = 56,
        SendDollar1000BillsInMeter = 57,
        SendDollar200BillsInMeter = 58,
        SendDollar25BillsInMeter = 59,
        SendDollar2000BillsInMeter = 60,
        SendCashOutTicketInformation = 61,
        SendDollar2500BillsInMeter = 62,
        SendDollar5000BillsInMeter = 63,
        SendDollar10000BillsInMeter = 64,
        SendDollar20000BillsInMeter = 65,
        SendDollar25000BillsInMeter = 66,
        SendDollar50000BillsInMeter = 67,
        SendDollar100000BillsInMeter = 68,
        SendDollar250BillsInMeter = 69,
        SendCreditOfAllBillsAccepted = 70,
        SendCoinFromExternalAcceptor = 71,
        SendLastAcceptedBillInformation = 72,
        SendNumberOfBillsInStacker = 73,
        SendTotalCreditOfBillsInStacker = 74,
        SetSecureEnhancedValidationId = 76,
        SendEnhancedValidationInformation = 77,
        SendCurrentHopperStatus = 79,
        SendValidationMeters = 80,
        SendTotalNumberOfGamesImplemented = 81,
        SendGameNMeters = 82,
        SendGameNConfiguration = 83,
        SendSasVersionIdAndMachineSerial = 84,
        SendSelectedGameNumber = 85,
        SendEnabledGameNumbers = 86,
        SendPendingCashoutInformation = 87,
        ReceiveValidationNumber = 88,
        SendAuthenticationInfo = 110,
        SendExtendedMetersForGameN = 111,
        SendTicketValidationData = 112,
        RedeemTicket = 113,
        AftTransferFunds = 114,
        AftRegisterGamingMachine = 115,
        AftGameLockAndStatusRequest = 116,
        SetAftReceiptData = 117,
        SetCustomAftTicketData = 118,
        ExtendedValidationStatus = 123,
        SetExtendedTicketData = 124,
        SetTicketData = 125,
        SendCurrentDateAndTime = 126,
        ReceiveDateAndTime = 127,
        ReceiveProgressiveAmount = 128,
        SendCumulativeProgressiveWins = 131,
        SendProgressiveWinAmount = 132,
        SendSasProgressiveWinAmount = 133,
        ReceiveMultipleProgressiveLevels = 134,
        SendMultipleSasProgressiveWin = 135,
        InitiateALegacyBonusPay = 138,
        InitiateMultipliedJackpotMode = 139,
        EnterExitTournamentMode = 140,
        SendCardInformation = 142,
        SendPhysicalReelStopInformation = 143,
        SendLegacyBonusWinAmounts = 144,
        RemoteHandpayReset = 148,
        SendTournamentGamesPlayed = 149,
        SendTournamentGamesWon = 150,
        SendTournamentCreditsWagered = 151,
        SendTournamentCreditsWon = 152,
        SendTournamentMeters95Through98 = 153,
        SendLegacyBonusMeters = 154,
        SendEnabledFeatures = 160,
        SendCashOutLimit = 164,
        EnableJackpotHandpayResetMethod = 168,
        EnableDisableGameAutoRebet = 170,
        SendExtendedMetersForGameNAlternate = 175,
        MultiDenominationPreamble = 176,
        SendCurrentPlayerDenomination = 177,
        SendEnabledPlayerDenomination = 178,
        SendTokenDenomination = 179,
        SendWagerCategoryInformation = 180,
        SendExtendedGameNInformation = 181,
        MeterCollectStatus = 182,
        SetMachineNumbers = 183,
        EventResponseToLongPoll = 255,
    }

    /// Table A-1: SAS Exception Codes
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum ExceptionCode {
        NoActivity = 0,
        SlotDoorWasOpened = 17,
        SlotDoorWasClosed = 18,
        DropDoorWasOpened = 19,
        DropDoorWasClosed = 20,
        CardCageWasOpened = 21,
        CardCageWasClosed = 22,
        AcPowerWasAppliedToGamingMachine = 23,
        AcPowerWasLostFromGamingMachine = 24,
        CashboxDoorWasOpened = 25,
        CashboxDoorWasClosed = 26,
        CashboxWasRemoved = 27,
        CashboxWasInstalled = 28,
        BellyDoorWasOpened = 29,
        BellyDoorWasClosed = 30,
        /// usElEss, shoulD not implEmEnt
        NoActivityAndWaitingForPlayerInput = 31,
        GeneralTilt = 32,
        CoinInTilt = 33,
        CoinOutTilt = 34,
        HopperEmptyDetected = 35,
        ExtraCoinPaid = 36,
        DiverterMalfunction = 37,
        CashboxFullDetected = 39,
        BillJam = 40,
        BillAcceptorHardwareFailure = 41,
        ReverseBillDetected = 42,
        BillRejected = 43,
        CounterfeitBillDetected = 44,
        ReverseCoinInDetected = 45,
        CashboxNearFullDetected = 46,
        /// ADDED in ADDEnDA_20080505
        BillAcceptorVersionChanged = 47,
        CmosRamErrorDataRecoveredFromEeprom = 49,
        CmosRamErrorNoDataRecoveredFromEeprom = 50,
        CmosRamErrorBadDevice = 51,
        EepromErrorDataError = 52,
        EepromErrorBadDevice = 53,
        EpromErrorDifferentChecksumVersionChanged = 54,
        EpromErrorBadChecksumCompare = 55,
        PartitionedEpromErrorChecksumVersionChanged = 56,
        PartitionedEpromErrorBadChecksumCompare = 57,
        MemoryErrorReset = 58,
        LowBackupBatteryDetected = 59,
        OperatorChangedOptions = 60,
        ACashOutTicketHasBeenPrinted = 61,
        AHandpayHasBeenValidated = 62,
        ValidationIdNotConfigured = 63,
        ReelTilt = 64,
        Reel1Tilt = 65,
        Reel2Tilt = 66,
        Reel3Tilt = 67,
        Reel4Tilt = 68,
        Reel5Tilt = 69,
        ReelMechanismDisconnected = 70,
        Dollar1BillAccepted = 71,
        Dollar5BillAccepted = 72,
        Dollar10BillAccepted = 73,
        Dollar20BillAccepted = 74,
        Dollar50BillAccepted = 75,
        Dollar100BillAccepted = 76,
        Dollar2BillAccepted = 77,
        Dollar500BillAccepted = 78,
        BillAccepted = 79,
        Dollar200BillAccepted = 80,
        HandpayIsPending = 81,
        HandpayWasReset = 82,
        NoProgressiveInformationHasBeenReceivedFor5Seconds = 83,
        ProgressiveWin = 84,
        PlayerHasCancelledTheHandpayRequest = 85,
        SasProgressiveLevelHit = 86,
        SystemValidationRequest = 87,
        ///EXCEPTION_CODE_TIP_AWARDED = 0x5F;
        PrinterCommunicationError = 96,
        PrinterPaperOutError = 97,
        CashOutButtonPressed = 102,
        TicketHasBeenInserted = 103,
        TicketTransferComplete = 104,
        AftTransferComplete = 105,
        AftRequestForHostCashout = 106,
        AftRequestForHostToCashOutWin = 107,
        AftRequestToRegister = 108,
        AftRegistrationAcknowledged = 109,
        AftRegistrationCancelled = 110,
        GameLocked = 111,
        ExceptionBufferOverflow = 112,
        ChangeLampOn = 113,
        ChangeLampOff = 114,
        PrinterPaperLow = 116,
        PrinterPowerOff = 117,
        PrinterPowerOn = 118,
        ReplacePrinterRibbon = 119,
        PrinterCarriageJammed = 120,
        CoinInLockoutMalfunction = 121,
        GamingMachineSoftMetersResetToZero = 122,
        BillValidatorTotalsHaveBeenResetByAnAttendant = 123,
        ALegacyBonusPayAwardedAndOrAMultipliedJackpotOccurred = 124,
        GameHasStarted = 126,
        GameHasEnded = 127,
        HopperFullDetected = 128,
        HopperLevelLowDetected = 129,
        DisplayMetersOrAttendantMenuHasBeenEntered = 130,
        DisplayMetersOrAttendantMenuHasBeenExited = 131,
        SelfTestOrOperatorMenuHasBeenEntered = 132,
        SelfTestOrOperatorMenuHasBeenExited = 133,
        GamingMachineIsOutOfService = 134,
        PlayerHasRequestedDrawCards = 135,
        ReelNHasStopped = 136,
        CoinCreditWagered = 137,
        GameRecallEntryHasBeenDisplayed = 138,
        CardHeldNotHeld = 139,
        GameSelected = 140,
        ComponentListChanged = 142,
        AuthenticationComplete = 143,
        PowerOffCardCageAccess = 152,
        PowerOffSlotDoorAccess = 153,
        PowerOffCashboxDoorAccess = 154,
        PowerOffDropDoorAccess = 155,
        SessionStart = 158,
        SessionStop = 159,
        /// EXCEPTION_CODE_METER_CHANGE_PENDING = 0xA0;
        /// EXCEPTION_CODE_METER_CHANGE_CANCELLED = 0xA1;
        /// EXCEPTION_CODE_ENABLED_GAMES_DENOMS_CHANGED = 0xA2;
        RealTimeEvent = 255,
    }

    /// Table C-7: SAS Meter Codes
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum MeterCode {
        /// @exclude GamingMachineMeters
        TotalCoinInCredits = 0,
        TotalCoinOutCredits = 1,
        TotalJackpotCredits = 2,
        TotalHandPaidCancelledCredits = 3,
        TotalCancelledCredits = 4,
        GamesPlayed = 5,
        GamesWon = 6,
        GamesLost = 7,
        TotalCreditsFromCoinAcceptor = 8,
        TotalCreditsPaidFromHopper = 9,
        TotalCreditsFromCoinsToDrop = 10,
        TotalCreditsFromBillsAccepted = 11,
        CurrentCredits = 12,
        /// @exclude SAS validation-specific meters for backwards compatibility only
        TotalSasCashableTicketInCents = 13,
        TotalSasCashableTicketOutCents = 14,
        TotalSasRestrictedTicketInCents = 15,
        TotalSasRestrictedTicketOutCents = 16,
        TotalSasCashableTicketInQuantity = 17,
        TotalSasCashableTicketOutQuantity = 18,
        TotalSasRestrictedTicketInQuantity = 19,
        TotalSasRestrictedTicketOutQuantity = 20,
        TotalTicketInCredits = 21,
        TotalTicketOutCredits = 22,
        TotalElectronicTransfersToGamingMachineCredits = 23,
        TotalElectronicTransfersToHostCredits = 24,
        TotalRestrictedAmountPlayedCredits = 25,
        TotalNonrestrictedAmountPlayedCredits = 26,
        CurrentRestrictedCredits = 27,
        TotalMachinePaidPaytableWinCredits = 28,
        TotalMachinePaidProgressiveWinCredits = 29,
        TotalMachinePaidExternalBonusWinCredits = 30,
        TotalAttendantPaidPaytableWinCredits = 31,
        TotalAttendantPaidProgressiveWinCredits = 32,
        TotalAttendantPaidExternalBonusWinCredits = 33,
        TotalWonCredits = 34,
        TotalHandPaidCredits = 35,
        TotalDropCredits = 36,
        GamesSinceLastPowerReset = 37,
        GamesSinceSlotDoorClosure = 38,
        TotalCreditsFromExternalCoinAcceptor = 39,
        TotalCashableTicketInCredits = 40,
        TotalRegularCashableTicketInCredits = 41,
        TotalRestrictedPromotionalTicketInCredits = 42,
        TotalNonrestrictedPromotionalTicketInCredits = 43,
        TotalCashableTicketOutCredits = 44,
        TotalRestrictedPromotionalTicketOutCredits = 45,
        ElectronicRegularCashableTransfersToGamingMachineCredits = 46,
        ElectronicRestrictedPromotionalTransfersToGamingMachineCredits = 47,
        ElectronicNonrestrictedPromotionalTransfersToGamingMachineCredits = 48,
        ElectronicDebitTransfersToGamingMachineCredits = 49,
        ElectronicRegularCashableTransfersToHostCredits = 50,
        ElectronicRestrictedPromotionalTransfersToHostCredits = 51,
        ElectronicNonrestrictedPromotionalTransfersToHostCredits = 52,
        TotalRegularCashableTicketInQuantity = 53,
        TotalRestrictedPromotionalTicketInQuantity = 54,
        TotalNonrestrictedPromotionalTicketInQuantity = 55,
        TotalCashableTicketOutQuantity = 56,
        TotalRestrictedPromotionalTicketOutQuantity = 57,
        /// @exclude BillsAcceptedMeters
        NumberOfBillsCurrentlyInTheStacker = 62,
        TotalValueOfBillsCurrentlyInTheStackerCredits = 63,
        TotalNumberOf1DollarBillsAccepted = 64,
        TotalNumberOf2DollarBillsAccepted = 65,
        TotalNumberOf5DollarBillsAccepted = 66,
        TotalNumberOf10DollarBillsAccepted = 67,
        TotalNumberOf20DollarBillsAccepted = 68,
        TotalNumberOf25DollarBillsAccepted = 69,
        TotalNumberOf50DollarBillsAccepted = 70,
        TotalNumberOf100DollarBillsAccepted = 71,
        TotalNumberOf200DollarBillsAccepted = 72,
        TotalNumberOf250DollarBillsAccepted = 73,
        TotalNumberOf500DollarBillsAccepted = 74,
        TotalNumberOf1000DollarBillsAccepted = 75,
        TotalNumberOf2000DollarBillsAccepted = 76,
        TotalNumberOf2500DollarBillsAccepted = 77,
        TotalNumberOf5000DollarBillsAccepted = 78,
        TotalNumberOf10000DollarBillsAccepted = 79,
        TotalNumberOf20000DollarBillsAccepted = 80,
        TotalNumberOf25000DollarBillsAccepted = 81,
        TotalNumberOf50000DollarBillsAccepted = 82,
        TotalNumberOf100000DollarBillsAccepted = 83,
        TotalNumberOf200000DollarBillsAccepted = 84,
        TotalNumberOf250000DollarBillsAccepted = 85,
        TotalNumberOf500000DollarBillsAccepted = 86,
        TotalNumberOf1000000DollarBillsAccepted = 87,
        /// @exclude BillsToDropMeters
        TotalCreditsFromDollarBillsToDrop = 88,
        TotalNumberOf1DollarBillsToDrop = 89,
        TotalNumberOf2DollarBillsToDrop = 90,
        TotalNumberOf5DollarBillsToDrop = 91,
        TotalNumberOf10DollarBillsToDrop = 92,
        TotalNumberOf20DollarBillsToDrop = 93,
        TotalNumberOf50DollarBillsToDrop = 94,
        TotalNumberOf100DollarBillsToDrop = 95,
        TotalNumberOf200DollarBillsToDrop = 96,
        TotalNumberOf500DollarBillsToDrop = 97,
        TotalNumberOf1000DollarBillsToDrop = 98,
        /// @exclude BillsDivertedToHopperMeters
        TotalCreditsFromDollarBillsDivertedToHopper = 99,
        TotalNumberOf1DollarBillsDivertedToHopper = 100,
        TotalNumberOf2DollarBillsDivertedToHopper = 101,
        TotalNumberOf5DollarBillsDivertedToHopper = 102,
        TotalNumberOf10DollarBillsDivertedToHopper = 103,
        TotalNumberOf20DollarBillsDivertedToHopper = 104,
        TotalNumberOf50DollarBillsDivertedToHopper = 105,
        TotalNumberOf100DollarBillsDivertedToHopper = 106,
        TotalNumberOf200DollarBillsDivertedToHopper = 107,
        TotalNumberOf500DollarBillsDivertedToHopper = 108,
        TotalNumberOf1000DollarBillsDivertedToHopper = 109,
        /// @exclude BillsDispensedFromHopperMeters
        TotalCreditsFromDollarBillsDispensedFromHopper = 110,
        TotalNumberOf1DollarBillsDispensedFromHopper = 111,
        TotalNumberOf2DollarBillsDispensedFromHopper = 112,
        TotalNumberOf5DollarBillsDispensedFromHopper = 113,
        TotalNumberOf10DollarBillsDispensedFromHopper = 114,
        TotalNumberOf20DollarBillsDispensedFromHopper = 115,
        TotalNumberOf50DollarBillsDispensedFromHopper = 116,
        TotalNumberOf100DollarBillsDispensedFromHopper = 117,
        TotalNumberOf200DollarBillsDispensedFromHopper = 118,
        TotalNumberOf500DollarBillsDispensedFromHopper = 119,
        TotalNumberOf1000DollarBillsDispensedFromHopper = 120,
        /// @exclude SAS 6.02 addon
        SessionsPlayed = 121,
        TipMoneyCredits = 122,
        WeightedAverageTheoreticalPaybackPercentage = 127,
        /// @exclude SAS Validation Specific Meters
        RegularCashableTicketInCents = 128,
        RegularCashableTicketInQuantity = 129,
        RestrictedTicketInCents = 130,
        RestrictedTicketInQuantity = 131,
        NonrestrictedTicketInCents = 132,
        NonrestrictedTicketInQuantity = 133,
        RegularCashableTicketOutCents = 134,
        RegularCashableTicketOutQuantity = 135,
        RestrictedTicketOutCents = 136,
        RestrictedTicketOutQuantity = 137,
        DebitTicketOutCents = 138,
        DebitTicketOutQuantity = 139,
        ValidatedCancelledCreditHandpayReceiptPrintedCents = 140,
        ValidatedCancelledCreditHandpayReceiptPrintedQuantity = 141,
        ValidatedJackpotHandpayReceiptPrintedCents = 142,
        ValidatedJackpotHandpayReceiptPrintedQuantity = 143,
        ValidatedCancelledCreditHandpayNoReceiptCents = 144,
        ValidatedCancelledCreditHandpayNoReceiptQuantity = 145,
        ValidatedJackpotHandpayNoReceiptCents = 146,
        ValidatedJackpotHandpayNoReceiptQuantity = 147,
        /// @exclude SAS AFT Specific Meters
        InHouseCashableTransfersToGamingMachineCents = 160,
        InHouseTransfersToGamingMachineThatIncludedCashableAmountsQuantity = 161,
        InHouseRestrictedTransfersToGamingMachineCents = 162,
        InHouseTransfersToGamingMachineThatIncludedRestrictedAmountsQuantity = 163,
        InHouseNonrestrictedTransfersToGamingMachineCents = 164,
        InHouseTransfersToGamingMachineThatIncludedNonrestrictedAmountsQuantity = 165,
        DebitTransfersToGamingMachineCents = 166,
        DebitTransfersToGamingMachineQuantity = 167,
        InHouseCashableTransfersToTicketCents = 168,
        InHouseCashableTransfersToTicketQuantity = 169,
        InHouseRestrictedTransfersToTicketCents = 170,
        InHouseRestrictedTransfersToTicketQuantity = 171,
        DebitTransfersToTicketCents = 172,
        DebitTransfersToTicketQuantity = 173,
        BonusCashableTransfersToGamingMachineCents = 174,
        BonusTransfersToGamingMachineThatIncludedCashableAmountsQuantity = 175,
        BonusNonrestrictedTransfersToGamingMachineCents = 176,
        BonusTransfersToGamingMachineThatIncludedNonrestrictedAmountsQuantity = 177,
        InHouseCashableTransfersToHostCents = 184,
        InHouseTransfersToHostThatIncludedCashableAmountsQuantity = 185,
        InHouseRestrictedTransfersToHostCents = 186,
        InHouseTransfersToHostThatIncludedRestrictedAmountsQuantity = 187,
        InHouseNonrestrictedTransfersToHostCents = 188,
        InHouseTransfersToHostThatIncludedNonrestrictedAmountsQuantity = 189,
        /// @exclude SAS 6.02 addon
        RegularCashableKeyedOnFundsCredits = 250,
        RestrictedKeyedOnFundsCredits = 251,
        NonrestrictedKeyedOnFundsCredits = 252,
        RegularCashableKeyedOffFundsCredits = 253,
        RestrictedKeyedOffFundsCredits = 254,
        NonrestrictedKeyedOffFundsCredits = 255,
    }

    /// Table C-6: Bill Denomination Codes
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum BillDenominationFlag {
        Invalid = 0,
        BillDenominationFlag1Dollar = 1,
        BillDenominationFlag2Dollar = 2,
        BillDenominationFlag5Dollar = 4,
        BillDenominationFlag10Dollar = 8,
        BillDenominationFlag20Dollar = 16,
        BillDenominationFlag25Dollar = 32,
        BillDenominationFlag50Dollar = 64,
        BillDenominationFlag100Dollar = 128,
        BillDenominationFlag200Dollar = 256,
        BillDenominationFlag250Dollar = 512,
        BillDenominationFlag500Dollar = 1024,
        BillDenominationFlag1000Dollar = 2048,
        BillDenominationFlag2000Dollar = 4096,
        BillDenominationFlag2500Dollar = 8192,
        BillDenominationFlag5000Dollar = 16384,
        BillDenominationFlag10000Dollar = 32768,
        BillDenominationFlag20000Dollar = 65536,
        BillDenominationFlag25000Dollar = 131072,
        BillDenominationFlag50000Dollar = 262144,
        BillDenominationFlag100000Dollar = 524288,
        BillDenominationFlag200000Dollar = 1048576,
        BillDenominationFlag250000Dollar = 2097152,
        BillDenominationFlag500000Dollar = 4194304,
        BillDenominationFlag1000000Dollar = 8388608,
    }

    /// Table C-4: Denomination
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum Denomination {
        None = 0,
        Cents1 = 1,
        Cents5 = 2,
        Cents10 = 3,
        Cents25 = 4,
        Cents50 = 5,
        Dollars1 = 6,
        Dollars5 = 7,
        Dollars10 = 8,
        Dollars20 = 9,
        Dollars100 = 10,
        Cents20 = 11,
        Dollars2 = 12,
        Dollars2Point5 = 13,
        Dollars25 = 14,
        Dollars50 = 15,
        Dollars200 = 16,
        Dollars250 = 17,
        Dollars500 = 18,
        Dollars1000 = 19,
        Dollars2000 = 20,
        Dollars2500 = 21,
        Dollars5000 = 22,
        Cents2 = 23,
        Cents3 = 24,
        Cents15 = 25,
        Cents40 = 26,
        CentsPoint500 = 27,
        CentsPoint250 = 28,
        CentsPoint200 = 29,
        CentsPoint100 = 30,
        CentsPoint050 = 31,
    }

    /// Table C-5
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum BillAcceptorCountryCode {
        Unknown = 0,
        Argentina = 1,
        Australia = 2,
        Austria = 3,
        Belgium = 4,
        Brazil = 5,
        Bulgaria = 6,
        Canada = 7,
        Columbia = 8,
        Cyprus = 9,
        Czechoslovakia = 10,
        Denmark = 11,
        Finland = 12,
        France = 13,
        Germany = 14,
        GreatBritain = 15,
        Gibraltar = 16,
        Greece = 17,
        Guernsey = 18,
        Hungary = 19,
        Ireland = 20,
        Italy = 21,
        Jersey = 22,
        Luxembourg = 23,
        Malta = 24,
        Mexico = 25,
        Morocco = 26,
        Norway = 27,
        Poland = 28,
        Portugal = 29,
        Romania = 30,
        Russia = 31,
        Spain = 32,
        SouthAfrica = 33,
        Sweden = 34,
        Switzerland = 35,
        Turkey = 36,
        UnitedStates = 37,
        Holland = 38,
        Euro = 39,
        Reserved = 40,
    }
// ============================================================================
// Section 8: AFT
// ============================================================================

    /// Table 8.3d
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum AftTransferType {
        InhouseHostToEgm = 0,
        CoinoutWinHostToEgm = 16,
        JackpotWinHostToEgm = 17,
        InhouseHostToTicket = 32,
        DebitHostToEgm = 64,
        DebitHostToTicket = 96,
        InhouseEgmToHost = 128,
        InhouseWinEgmToHost = 144,
    }

    /// Table 8.3e
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum AftTransferStatusCode {
        FullTransferSuccessful = 0,
        PartialTransferSuccessful = 1,
        TransferPending = 64,
        CancelledByHost = 128,
        TransactionIdNotUnique = 129,
        InvalidTransferFunction = 130,
        InvalidAmountOrExpiration = 131,
        AmountExceedsTransferLimit = 132,
        AmountNotMultipleOfDenomination = 133,
        UnableToPerformPartialTransferToHost = 134,
        UnableToPerformPartialTransferNow = 135,
        EgmNotRegistered = 136,
        RegistrationKeyMismatch = 137,
        NoPosId = 138,
        NoWonCreditsAvailableForCashout = 139,
        EgmDenominationNotSet = 140,
        InvalidTicketExpiration = 141,
        TransferToTicketNotAvailable = 142,
        RestrictedPoolIdMismatch = 143,
        UnableToPrintTransactionReceipt = 144,
        InsufficientDataToPrintTransactionReceipt = 145,
        TransactionReceiptNotAllowedForTransferType = 146,
        AssetNumberMismatch = 147,
        EgmNotLocked = 148,
        InvalidTransactionId = 149,
        UnexpectedError = 159,
        IncompatibleWithTransferInProgress = 192,
        UnsupportedTransferCode = 193,
        NoInformationAvailable = 255,
    }

    /// Table 8.3g
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum AftTransactionReceiptStatus {
        Printed = 0,
        Printing = 32,
        Pending = 64,
        None = 255,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum AftTicketFieldId {
        Location = 0,
        Address1 = 1,
        Address2 = 2,
        GraphicsSelector = 3,
        Title = 16,
    }
// ============================================================================
// Section 12: Real-time Event Reporting
// ============================================================================

    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum TaxStatus {
        Deductible = 0,
        Nondeductible = 1,
        WagerMatch = 2,
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum ExtendedTicketField {
        Location = 0,
        Address1 = 1,
        Address2 = 2,
        GraphicsSelector = 3,
        Title = 16,
    }
// ----------------------------------------------------------------------------
// 15.10
// ----------------------------------------------------------------------------

    /// Table 15.13c
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum ValidationType {
        CashableTicketCashoutOrWinNoHandpayLockup = 0,
        RestrictedPromotionalTicketFromCashout = 1,
        CashableTicketFromAftTransfer = 2,
        RestrictedTicketFromAftTransfer = 3,
        DebitTicketFromAftTransfer = 4,
        CancelledCreditHandpayReceiptPrinted = 16,
        JackpotHandpayReceiptPrinted = 32,
        CancelledCreditHandpayNoReceipt = 64,
        JackpotHandpayNoReceipt = 96,
        CashableTicketRedeemed = 128,
        RestrictedPromotionalTicketRedeemed = 129,
        NonrestrictedPromotionalTicketRedeemed = 130,
    }
// ----------------------------------------------------------------------------
// 15.12 Redeem Ticket
// ----------------------------------------------------------------------------

    /// Table 15.12c
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum TicketTransferCode {
        ValidCashableTicket = 0,
        ValidRestrictedPromotionalTicket = 1,
        ValidNonrestrictedPromotionalTicket = 2,
        UnableToValidate = 128,
        InvalidValidationNumber = 129,
        ValidationNumberNotInSystem = 130,
        PendingInSystem = 131,
        AlreadyRedeemed = 132,
        Expired = 133,
        ValidationInfoNotAvailable = 134,
        AmountMismatch = 135,
        AmountExceedsRedemptionLimit = 136,
        RequestCurrentTicketStatus = 255,
    }

    /// Table 15.12d
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum EgmTicketRedemptionStatus {
        CashableTicketRedeemed = 0,
        RestrictedPromotionalTicketRedeemed = 1,
        NonrestrictedPromotionalTicketRedeemed = 2,
        WaitingForLongPoll71 = 32,
        Pending = 64,
        Rejected = 128,
        ValidationNumberMismatch = 129,
        InvalidTransferFunction = 130,
        InvalidAmount = 131,
        AmountExceedsCreditLimit = 132,
        AmountNotValidForDenomination = 133,
        TransferAmountDoesNotMatchTicketAmount = 134,
        UnableToAcceptTransferAtThisTime = 135,
        RejectedTimeout = 136,
        RejectedLinkDown = 137,
        Disabled = 138,
        RejectedValidatorFailure = 139,
        IncompatibleWithRedemptionCycle = 192,
        ValidationInformationUnavailable = 255,
    }
// ============================================================================
// Section 16: Multidenom extensions
// ============================================================================

// ----------------------------------------------------------------------------
// 16.1 Multidenom preamble
// ----------------------------------------------------------------------------

    /// Table 16.1d
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum MultidenomPreambleError {
        Success = 0,
        Unsupported = 1,
        Malformed = 2,
        Invalid = 3,
        UnsupportedForDenomination = 4,
        InvalidDenomination = 5,
    }
// ============================================================================
// Section 17: Component Authentication
// ============================================================================

    /// Table 17.1c
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum AuthenticationMethod {
        None = 0,
        Crc16 = 1,
        Crc32 = 2,
        Md5 = 4,
        KobetronI = 8,
        KobetronIi = 16,
        Sha1 = 32,
    }
}

