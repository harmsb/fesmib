use bytes::{Bytes, Buf, BufMut};
use bytes::buf::ext::BufExt;
use num_enum::{TryFromPrimitive, IntoPrimitive};
use std::convert::{TryFrom};

#[derive(Debug, TryFromPrimitive, IntoPrimitive, Eq, PartialEq, Copy, Clone)]
#[repr(u8)]
pub enum LongPollId {
    Unknown = 0,
    Shutdown = 1,
    Startup = 2,
    SoundOff = 3,
    SoundOn = 4,
    ReelSpinOrGamePlaySoundsDisabled = 5,
    EnableBillAcceptor = 6,
    DisableBillAcceptor = 7,
    ConfigureBillDenominations = 8,
    EnableDisableGameN = 9,
    EnterMaintenanceMode = 10,
    ExitMaintenanceMode = 11,
    EnableDisableRealTimeEventReporting = 14,
    SendMeters10Through15 = 15,
    SendTotalCanceledCreditsMeter = 16,
    SendTotalCoinInMeter = 17,
    SendTotalCoinOutMeter = 18,
    SendTotalDropMeter = 19,
    SendTotalJackpotMeter = 20,
    SendGamesPlayedMeter = 21,
    SendGamesWonMeter = 22,
    SendGamesLostMeter = 23,
    SendGamesSincePowerUpDoorClosure = 24,
    SendMeters11Through15 = 25,
    SendCurrentCredits = 26,
    SendHandpayInformation = 27,
    SendMeters = 28,
    SendTotalBillMeters = 30,
    SendGamingMachineIdAndInformation = 31,
    SendTotalDollarValueOfBillsMeter = 32,
    RomSignatureVerification = 33,
    SendTrueCoinIn = 42,
    SendTrueCoinOut = 43,
    SendCurrentHopperLevel = 44,
    SendTotalHandPaidCancelledCredits = 45,
    DelayGame = 46,
    SendSelectedMetersForGameN = 47,
    SendDollar1BillsInMeter = 49,
    SendDollar2BillsInMeter = 50,
    SendDollar5BillsInMeter = 51,
    SendDollar10BillsInMeter = 52,
    SendDollar20BillsInMeter = 53,
    SendDollar50BillsInMeter = 54,
    SendDollar100BillsInMeter = 55,
    SendDollar500BillsInMeter = 56,
    SendDollar1000BillsInMeter = 57,
    SendDollar200BillsInMeter = 58,
    SendDollar25BillsInMeter = 59,
    SendDollar2000BillsInMeter = 60,
    SendCashOutTicketInformation = 61,
    SendDollar2500BillsInMeter = 62,
    SendDollar5000BillsInMeter = 63,
    SendDollar10000BillsInMeter = 64,
    SendDollar20000BillsInMeter = 65,
    SendDollar25000BillsInMeter = 66,
    SendDollar50000BillsInMeter = 67,
    SendDollar100000BillsInMeter = 68,
    SendDollar250BillsInMeter = 69,
    SendCreditOfAllBillsAccepted = 70,
    SendCoinFromExternalAcceptor = 71,
    SendLastAcceptedBillInformation = 72,
    SendNumberOfBillsInStacker = 73,
    SendTotalCreditOfBillsInStacker = 74,
    SetSecureEnhancedValidationId = 76,
    SendEnhancedValidationInformation = 77,
    SendCurrentHopperStatus = 79,
    SendValidationMeters = 80,
    SendTotalNumberOfGamesImplemented = 81,
    SendGameNMeters = 82,
    SendGameNConfiguration = 83,
    SendSasVersionIdAndMachineSerial = 84,
    SendSelectedGameNumber = 85,
    SendEnabledGameNumbers = 86,
    SendPendingCashoutInformation = 87,
    ReceiveValidationNumber = 88,
    SendAuthenticationInfo = 110,
    SendExtendedMetersForGameN = 111,
    SendTicketValidationData = 112,
    RedeemTicket = 113,
    AftTransferFunds = 114,
    AftRegisterGamingMachine = 115,
    AftGameLockAndStatusRequest = 116,
    SetAftReceiptData = 117,
    SetCustomAftTicketData = 118,
    ExtendedValidationStatus = 123,
    SetExtendedTicketData = 124,
    SetTicketData = 125,
    SendCurrentDateAndTime = 126,
    ReceiveDateAndTime = 127,
    ReceiveProgressiveAmount = 128,
    SendCumulativeProgressiveWins = 131,
    SendProgressiveWinAmount = 132,
    SendSasProgressiveWinAmount = 133,
    ReceiveMultipleProgressiveLevels = 134,
    SendMultipleSasProgressiveWin = 135,
    InitiateALegacyBonusPay = 138,
    InitiateMultipliedJackpotMode = 139,
    EnterExitTournamentMode = 140,
    SendCardInformation = 142,
    SendPhysicalReelStopInformation = 143,
    SendLegacyBonusWinAmounts = 144,
    RemoteHandpayReset = 148,
    SendTournamentGamesPlayed = 149,
    SendTournamentGamesWon = 150,
    SendTournamentCreditsWagered = 151,
    SendTournamentCreditsWon = 152,
    SendTournamentMeters95Through98 = 153,
    SendLegacyBonusMeters = 154,
    SendEnabledFeatures = 160,
    SendCashOutLimit = 164,
    EnableJackpotHandpayResetMethod = 168,
    EnableDisableGameAutoRebet = 170,
    SendExtendedMetersForGameNAlternate = 175,
    MultiDenominationPreamble = 176,
    SendCurrentPlayerDenomination = 177,
    SendEnabledPlayerDenomination = 178,
    SendTokenDenomination = 179,
    SendWagerCategoryInformation = 180,
    SendExtendedGameNInformation = 181,
    MeterCollectStatus = 182,
    SetMachineNumbers = 183,
    EventResponseToLongPoll = 255
}

pub static METER_SIZES: [usize; 256] = [
    4,   // METER_CODE_TOTAL_COIN_IN_CREDITS = 0,
    4,   // METER_CODE_TOTAL_COIN_OUT_CREDITS = 1,
    4,   // METER_CODE_TOTAL_JACKPOT_CREDITS = 2,
    4,   // METER_CODE_TOTAL_HAND_PAID_CANCELLED_CREDITS = 3,
    4,   // METER_CODE_TOTAL_CANCELLED_CREDITS = 4,
    4,   // METER_CODE_GAMES_PLAYED = 5,
    4,   // METER_CODE_GAMES_WON = 6,
    4,   // METER_CODE_GAMES_LOST = 7,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_COIN_ACCEPTOR = 8,
    4,   // METER_CODE_TOTAL_CREDITS_PAID_FROM_HOPPER = 9,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_COINS_TO_DROP = 10,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_BILLS_ACCEPTED = 11,
    4,   // METER_CODE_CURRENT_CREDITS = 12,
    5,   // METER_CODE_TOTAL_SAS_CASHABLE_TICKET_IN_CENTS = 13,
    5,   // METER_CODE_TOTAL_SAS_CASHABLE_TICKET_OUT_CENTS = 14,
    5,   // METER_CODE_TOTAL_SAS_RESTRICTED_TICKET_IN_CENTS = 15,
    5,   // METER_CODE_TOTAL_SAS_RESTRICTED_TICKET_OUT_CENTS = 16,
    4,   // METER_CODE_TOTAL_SAS_CASHABLE_TICKET_IN_QUANTITY = 17,
    4,   // METER_CODE_TOTAL_SAS_CASHABLE_TICKET_OUT_QUANTITY = 18,
    4,   // METER_CODE_TOTAL_SAS_RESTRICTED_TICKET_IN_QUANTITY = 19,
    4,   // METER_CODE_TOTAL_SAS_RESTRICTED_TICKET_OUT_QUANTITY = 20,
    4,   // METER_CODE_TOTAL_TICKET_IN_CREDITS = 21,
    4,   // METER_CODE_TOTAL_TICKET_OUT_CREDITS = 22,
    4,   // METER_CODE_TOTAL_ELECTRONIC_TRANSFERS_TO_GAMING_MACHINE_CREDITS = 23,
    4,   // METER_CODE_TOTAL_ELECTRONIC_TRANSFERS_TO_HOST_CREDITS = 24,
    4,   // METER_CODE_TOTAL_RESTRICTED_AMOUNT_PLAYED_CREDITS = 25,
    4,   // METER_CODE_TOTAL_NONRESTRICTED_AMOUNT_PLAYED_CREDITS = 26,
    4,   // METER_CODE_CURRENT_RESTRICTED_CREDITS = 27,
    4,   // METER_CODE_TOTAL_MACHINE_PAID_PAYTABLE_WIN_CREDITS = 28,
    4,   // METER_CODE_TOTAL_MACHINE_PAID_PROGRESSIVE_WIN_CREDITS = 29,
    4,   // METER_CODE_TOTAL_MACHINE_PAID_EXTERNAL_BONUS_WIN_CREDITS = 30,
    4,   // METER_CODE_TOTAL_ATTENDANT_PAID_PAYTABLE_WIN_CREDITS = 31,
    4,   // METER_CODE_TOTAL_ATTENDANT_PAID_PROGRESSIVE_WIN_CREDITS = 32,
    4,   // METER_CODE_TOTAL_ATTENDANT_PAID_EXTERNAL_BONUS_WIN_CREDITS = 33,
    4,   // METER_CODE_TOTAL_WON_CREDITS = 34,
    4,   // METER_CODE_TOTAL_HAND_PAID_CREDITS = 35,
    4,   // METER_CODE_TOTAL_DROP_CREDITS = 36,
    4,   // METER_CODE_GAMES_SINCE_LAST_POWER_RESET = 37,
    4,   // METER_CODE_GAMES_SINCE_SLOT_DOOR_CLOSURE = 38,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_EXTERNAL_COIN_ACCEPTOR = 39,
    4,   // METER_CODE_TOTAL_CASHABLE_TICKET_IN_CREDITS = 40,
    4,   // METER_CODE_TOTAL_REGULAR_CASHABLE_TICKET_IN_CREDITS = 41,
    4,   // METER_CODE_TOTAL_RESTRICTED_PROMOTIONAL_TICKET_IN_CREDITS = 42,
    4,   // METER_CODE_TOTAL_NONRESTRICTED_PROMOTIONAL_TICKET_IN_CREDITS = 43,
    4,   // METER_CODE_TOTAL_CASHABLE_TICKET_OUT_CREDITS = 44,
    4,   // METER_CODE_TOTAL_RESTRICTED_PROMOTIONAL_TICKET_OUT_CREDITS = 45,
    4,   // METER_CODE_ELECTRONIC_REGULAR_CASHABLE_TRANSFERS_TO_GAMING_MACHINE_CREDITS = 46,
    4,   // METER_CODE_ELECTRONIC_RESTRICTED_PROMOTIONAL_TRANSFERS_TO_GAMING_MACHINE_CREDITS = 47,
    4,   // METER_CODE_ELECTRONIC_NONRESTRICTED_PROMOTIONAL_TRANSFERS_TO_GAMING_MACHINE_CREDITS = 48,
    4,   // METER_CODE_ELECTRONIC_DEBIT_TRANSFERS_TO_GAMING_MACHINE_CREDITS = 49,
    4,   // METER_CODE_ELECTRONIC_REGULAR_CASHABLE_TRANSFERS_TO_HOST_CREDITS = 50,
    4,   // METER_CODE_ELECTRONIC_RESTRICTED_PROMOTIONAL_TRANSFERS_TO_HOST_CREDITS = 51,
    4,   // METER_CODE_ELECTRONIC_NONRESTRICTED_PROMOTIONAL_TRANSFERS_TO_HOST_CREDITS = 52,
    4,   // METER_CODE_TOTAL_REGULAR_CASHABLE_TICKET_IN_QUANTITY = 53,
    4,   // METER_CODE_TOTAL_RESTRICTED_PROMOTIONAL_TICKET_IN_QUANTITY = 54,
    4,   // METER_CODE_TOTAL_NONRESTRICTED_PROMOTIONAL_TICKET_IN_QUANTITY = 55,
    4,   // METER_CODE_TOTAL_CASHABLE_TICKET_OUT_QUANTITY = 56,
    4,   // METER_CODE_TOTAL_RESTRICTED_PROMOTIONAL_TICKET_OUT_QUANTITY = 57,
    0,   // 58
    0,   // 59
    0,   // 60
    0,   // 61
    4,   // METER_CODE_NUMBER_OF_BILLS_CURRENTLY_IN_THE_STACKER = 62,
    4,   // METER_CODE_TOTAL_VALUE_OF_BILLS_CURRENTLY_IN_THE_STACKER_CREDITS = 63,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1_DOLLAR_BILLS_ACCEPTED = 64,
    4,   // METER_CODE_TOTAL_NUMBER_OF_2_DOLLAR_BILLS_ACCEPTED = 65,
    4,   // METER_CODE_TOTAL_NUMBER_OF_5_DOLLAR_BILLS_ACCEPTED = 66,
    4,   // METER_CODE_TOTAL_NUMBER_OF_10_DOLLAR_BILLS_ACCEPTED = 67,
    4,   // METER_CODE_TOTAL_NUMBER_OF_20_DOLLAR_BILLS_ACCEPTED = 68,
    4,   // METER_CODE_TOTAL_NUMBER_OF_25_DOLLAR_BILLS_ACCEPTED = 69,
    4,   // METER_CODE_TOTAL_NUMBER_OF_50_DOLLAR_BILLS_ACCEPTED = 70,
    4,   // METER_CODE_TOTAL_NUMBER_OF_100_DOLLAR_BILLS_ACCEPTED = 71,
    4,   // METER_CODE_TOTAL_NUMBER_OF_200_DOLLAR_BILLS_ACCEPTED = 72,
    4,   // METER_CODE_TOTAL_NUMBER_OF_250_DOLLAR_BILLS_ACCEPTED = 73,
    4,   // METER_CODE_TOTAL_NUMBER_OF_500_DOLLAR_BILLS_ACCEPTED = 74,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1000_DOLLAR_BILLS_ACCEPTED = 75,
    4,   // METER_CODE_TOTAL_NUMBER_OF_2000_DOLLAR_BILLS_ACCEPTED = 76,
    4,   // METER_CODE_TOTAL_NUMBER_OF_2500_DOLLAR_BILLS_ACCEPTED = 77,
    4,   // METER_CODE_TOTAL_NUMBER_OF_5000_DOLLAR_BILLS_ACCEPTED = 78,
    4,   // METER_CODE_TOTAL_NUMBER_OF_10000_DOLLAR_BILLS_ACCEPTED = 79,
    4,   // METER_CODE_TOTAL_NUMBER_OF_20000_DOLLAR_BILLS_ACCEPTED = 80,
    4,   // METER_CODE_TOTAL_NUMBER_OF_25000_DOLLAR_BILLS_ACCEPTED = 81,
    4,   // METER_CODE_TOTAL_NUMBER_OF_50000_DOLLAR_BILLS_ACCEPTED = 82,
    4,   // METER_CODE_TOTAL_NUMBER_OF_100000_DOLLAR_BILLS_ACCEPTED = 83,
    4,   // METER_CODE_TOTAL_NUMBER_OF_200000_DOLLAR_BILLS_ACCEPTED = 84,
    4,   // METER_CODE_TOTAL_NUMBER_OF_250000_DOLLAR_BILLS_ACCEPTED = 85,
    4,   // METER_CODE_TOTAL_NUMBER_OF_500000_DOLLAR_BILLS_ACCEPTED = 86,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1000000_DOLLAR_BILLS_ACCEPTED = 87,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_DOLLAR_BILLS_TO_DROP = 88,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1_DOLLAR_BILLS_TO_DROP = 89,
    4,   // METER_CODE_TOTAL_NUMBER_OF_2_DOLLAR_BILLS_TO_DROP = 90,
    4,   // METER_CODE_TOTAL_NUMBER_OF_5_DOLLAR_BILLS_TO_DROP = 91,
    4,   // METER_CODE_TOTAL_NUMBER_OF_10_DOLLAR_BILLS_TO_DROP = 92,
    4,   // METER_CODE_TOTAL_NUMBER_OF_20_DOLLAR_BILLS_TO_DROP = 93,
    4,   // METER_CODE_TOTAL_NUMBER_OF_50_DOLLAR_BILLS_TO_DROP = 94,
    4,   // METER_CODE_TOTAL_NUMBER_OF_100_DOLLAR_BILLS_TO_DROP = 95,
    4,   // METER_CODE_TOTAL_NUMBER_OF_200_DOLLAR_BILLS_TO_DROP = 96,
    4,   // METER_CODE_TOTAL_NUMBER_OF_500_DOLLAR_BILLS_TO_DROP = 97,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1000_DOLLAR_BILLS_TO_DROP = 98,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 99,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 100,
    4,   // METER_CODE_TOTAL_NUMBER_OF_2_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 101,
    4,   // METER_CODE_TOTAL_NUMBER_OF_5_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 102,
    4,   // METER_CODE_TOTAL_NUMBER_OF_10_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 103,
    4,   // METER_CODE_TOTAL_NUMBER_OF_20_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 104,
    4,   // METER_CODE_TOTAL_NUMBER_OF_50_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 105,
    4,   // METER_CODE_TOTAL_NUMBER_OF_100_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 106,
    4,   // METER_CODE_TOTAL_NUMBER_OF_200_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 107,
    4,   // METER_CODE_TOTAL_NUMBER_OF_500_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 108,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1000_DOLLAR_BILLS_DIVERTED_TO_HOPPER = 109,
    4,   // METER_CODE_TOTAL_CREDITS_FROM_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 110,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 111,
    4,   // METER_CODE_TOTAL_NUMBER_OF_2_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 112,
    4,   // METER_CODE_TOTAL_NUMBER_OF_5_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 113,
    4,   // METER_CODE_TOTAL_NUMBER_OF_10_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 114,
    4,   // METER_CODE_TOTAL_NUMBER_OF_20_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 115,
    4,   // METER_CODE_TOTAL_NUMBER_OF_50_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 116,
    4,   // METER_CODE_TOTAL_NUMBER_OF_100_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 117,
    4,   // METER_CODE_TOTAL_NUMBER_OF_200_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 118,
    4,   // METER_CODE_TOTAL_NUMBER_OF_500_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 119,
    4,   // METER_CODE_TOTAL_NUMBER_OF_1000_DOLLAR_BILLS_DISPENSED_FROM_HOPPER = 120,
    4,   // METER_CODE_SESSIONS_PLAYED = 121,
    4,   // METER_CODE_TIP_MONEY_CREDITS = 122,
    0,   // 123
    0,   // 124
    0,   // 125
    0,   // 126
    4,   // METER_CODE_WEIGHTED_AVERAGE_THEORETICAL_PAYBACK_PERCENTAGE = 127,
    5,   // METER_CODE_REGULAR_CASHABLE_TICKET_IN_CENTS = 128,
    4,   // METER_CODE_REGULAR_CASHABLE_TICKET_IN_QUANTITY = 129,
    5,   // METER_CODE_RESTRICTED_TICKET_IN_CENTS = 130,
    4,   // METER_CODE_RESTRICTED_TICKET_IN_QUANTITY = 131,
    5,   // METER_CODE_NONRESTRICTED_TICKET_IN_CENTS = 132,
    4,   // METER_CODE_NONRESTRICTED_TICKET_IN_QUANTITY = 133,
    5,   // METER_CODE_REGULAR_CASHABLE_TICKET_OUT_CENTS = 134,
    4,   // METER_CODE_REGULAR_CASHABLE_TICKET_OUT_QUANTITY = 135,
    5,   // METER_CODE_RESTRICTED_TICKET_OUT_CENTS = 136,
    4,   // METER_CODE_RESTRICTED_TICKET_OUT_QUANTITY = 137,
    5,   // METER_CODE_DEBIT_TICKET_OUT_CENTS = 138,
    4,   // METER_CODE_DEBIT_TICKET_OUT_QUANTITY = 139,
    5,   // METER_CODE_VALIDATED_CANCELLED_CREDIT_HANDPAY_RECEIPT_PRINTED_CENTS = 140,
    4,   // METER_CODE_VALIDATED_CANCELLED_CREDIT_HANDPAY_RECEIPT_PRINTED_QUANTITY = 141,
    5,   // METER_CODE_VALIDATED_JACKPOT_HANDPAY_RECEIPT_PRINTED_CENTS = 142,
    4,   // METER_CODE_VALIDATED_JACKPOT_HANDPAY_RECEIPT_PRINTED_QUANTITY = 143,
    5,   // METER_CODE_VALIDATED_CANCELLED_CREDIT_HANDPAY_NO_RECEIPT_CENTS = 144,
    4,   // METER_CODE_VALIDATED_CANCELLED_CREDIT_HANDPAY_NO_RECEIPT_QUANTITY = 145,
    5,   // METER_CODE_VALIDATED_JACKPOT_HANDPAY_NO_RECEIPT_CENTS = 146,
    4,   // METER_CODE_VALIDATED_JACKPOT_HANDPAY_NO_RECEIPT_QUANTITY = 147,
    0,   // 148
    0,   // 149
    0,   // 150
    0,   // 151
    0,   // 152
    0,   // 153
    0,   // 154
    0,   // 155
    0,   // 156
    0,   // 157
    0,   // 158
    0,   // 159
    5,   // METER_CODE_IN_HOUSE_CASHABLE_TRANSFERS_TO_GAMING_MACHINE_CENTS = 160,
    4,   // METER_CODE_IN_HOUSE_TRANSFERS_TO_GAMING_MACHINE_THAT_INCLUDED_CASHABLE_AMOUNTS_QUANTITY = 161,
    5,   // METER_CODE_IN_HOUSE_RESTRICTED_TRANSFERS_TO_GAMING_MACHINE_CENTS = 162,
    4,   // METER_CODE_IN_HOUSE_TRANSFERS_TO_GAMING_MACHINE_THAT_INCLUDED_RESTRICTED_AMOUNTS_QUANTITY = 163,
    5,   // METER_CODE_IN_HOUSE_NONRESTRICTED_TRANSFERS_TO_GAMING_MACHINE_CENTS = 164,
    4,   // METER_CODE_IN_HOUSE_TRANSFERS_TO_GAMING_MACHINE_THAT_INCLUDED_NONRESTRICTED_AMOUNTS_QUANTITY = 165,
    5,   // METER_CODE_DEBIT_TRANSFERS_TO_GAMING_MACHINE_CENTS = 166,
    4,   // METER_CODE_DEBIT_TRANSFERS_TO_GAMING_MACHINE_QUANTITY = 167,
    5,   // METER_CODE_IN_HOUSE_CASHABLE_TRANSFERS_TO_TICKET_CENTS = 168,
    4,   // METER_CODE_IN_HOUSE_CASHABLE_TRANSFERS_TO_TICKET_QUANTITY = 169,
    5,   // METER_CODE_IN_HOUSE_RESTRICTED_TRANSFERS_TO_TICKET_CENTS = 170,
    4,   // METER_CODE_IN_HOUSE_RESTRICTED_TRANSFERS_TO_TICKET_QUANTITY = 171,
    5,   // METER_CODE_DEBIT_TRANSFERS_TO_TICKET_CENTS = 172,
    4,   // METER_CODE_DEBIT_TRANSFERS_TO_TICKET_QUANTITY = 173,
    5,   // METER_CODE_BONUS_CASHABLE_TRANSFERS_TO_GAMING_MACHINE_CENTS = 174,
    4,   // METER_CODE_BONUS_TRANSFERS_TO_GAMING_MACHINE_THAT_INCLUDED_CASHABLE_AMOUNTS_QUANTITY = 175,
    5,   // METER_CODE_BONUS_NONRESTRICTED_TRANSFERS_TO_GAMING_MACHINE_CENTS = 176,
    4,   // METER_CODE_BONUS_TRANSFERS_TO_GAMING_MACHINE_THAT_INCLUDED_NONRESTRICTED_AMOUNTS_QUANTITY = 177,
    0,   // 178
    0,   // 179
    0,   // 180
    0,   // 181
    0,   // 182
    0,   // 183
    5,   // METER_CODE_IN_HOUSE_CASHABLE_TRANSFERS_TO_HOST_CENTS = 184,
    4,   // METER_CODE_IN_HOUSE_TRANSFERS_TO_HOST_THAT_INCLUDED_CASHABLE_AMOUNTS_QUANTITY = 185,
    5,   // METER_CODE_IN_HOUSE_RESTRICTED_TRANSFERS_TO_HOST_CENTS = 186,
    4,   // METER_CODE_IN_HOUSE_TRANSFERS_TO_HOST_THAT_INCLUDED_RESTRICTED_AMOUNTS_QUANTITY = 187,
    5,   // METER_CODE_IN_HOUSE_NONRESTRICTED_TRANSFERS_TO_HOST_CENTS = 188,
    4,   // METER_CODE_IN_HOUSE_TRANSFERS_TO_HOST_THAT_INCLUDED_NONRESTRICTED_AMOUNTS_QUANTITY = 189,
    0,   // 190
    0,   // 191
    0,   // 192
    0,   // 193
    0,   // 194
    0,   // 195
    0,   // 196
    0,   // 197
    0,   // 198
    0,   // 199
    0,   // 200
    0,   // 201
    0,   // 202
    0,   // 203
    0,   // 204
    0,   // 205
    0,   // 206
    0,   // 207
    0,   // 208
    0,   // 209
    0,   // 210
    0,   // 211
    0,   // 212
    0,   // 213
    0,   // 214
    0,   // 215
    0,   // 216
    0,   // 217
    0,   // 218
    0,   // 219
    0,   // 220
    0,   // 221
    0,   // 222
    0,   // 223
    0,   // 224
    0,   // 225
    0,   // 226
    0,   // 227
    0,   // 228
    0,   // 229
    0,   // 230
    0,   // 231
    0,   // 232
    0,   // 233
    0,   // 234
    0,   // 235
    0,   // 236
    0,   // 237
    0,   // 238
    0,   // 239
    0,   // 240
    0,   // 241
    0,   // 242
    0,   // 243
    0,   // 244
    0,   // 245
    0,   // 246
    0,   // 247
    0,   // 248
    0,   // 249
    4,   // METER_CODE_REGULAR_CASHABLE_KEYED_ON_FUNDS_CREDITS = 250,
    4,   // METER_CODE_RESTRICTED_KEYED_ON_FUNDS_CREDITS = 251,
    4,   // METER_CODE_NONRESTRICTED_KEYED_ON_FUNDS_CREDITS = 252,
    4,   // METER_CODE_REGULAR_CASHABLE_KEYED_OFF_FUNDS_CREDITS = 253,
    4,   // METER_CODE_RESTRICTED_KEYED_OFF_FUNDS_CREDITS = 254,
    4,   // METER_CODE_NONRESTRICTED_KEYED_OFF_FUNDS_CREDITS = 255
];

pub fn encode_bcd_byte(value: u8) -> u8 {
    (value % 10) | ((value / 10) << 4)
}

pub fn encode_bcd(src: u64, dest_size: usize) -> Vec<u8> {
    let mut bcd_buf = vec![0; dest_size];
    let mut v = src;

    for i in (0..dest_size).rev() {
        bcd_buf[i] = encode_bcd_byte((v % 100) as u8);
        v /= 100;
    }

    bcd_buf
}

pub fn decode_bcd_byte(bcd: u8) -> u8 {
    return (bcd & 0x0F) + ((bcd >> 4) * 10);
}

pub fn decode_bcd(src: &[u8]) -> u64 {
    src.iter().fold(0, |acc, &b| (acc * 100) + decode_bcd_byte(b) as u64)
}

pub fn calc_crc(src: &[u8]) -> u16  {
    calc_crc_iter(src.iter())
}



pub fn calc_crc_iter<'a>(iter: impl Iterator<Item=&'a u8>) -> u16  {
    let mut q: u32 = 0;
    let mut crc: u32 = 0;

    iter.for_each(|b| {
        let c = *b as u32;
        q = (crc ^ c) & 0o17;
        crc = (crc >> 4) ^ (q * 0o10201);
        q = (crc ^ (c >> 4)) & 0o17;
        crc = (crc >> 4) ^ (q * 0o10201);
    });

    (crc & 0xFFFF) as u16
}

pub trait SasSerialMessageBuilder: BufMut {
    const PROGRESSIVE_LEVEL_AMOUNT_LEVEL_SIZE: usize = 1;
    const PROGRESSIVE_LEVEL_AMOUNT_BCD_SIZE: usize = 5;
    const PROGRESSIVE_LEVEL_AMOUNT_SIZE: usize = Self::PROGRESSIVE_LEVEL_AMOUNT_LEVEL_SIZE +
                                                 Self::PROGRESSIVE_LEVEL_AMOUNT_BCD_SIZE + 1;

    fn with_long_poll(&mut self, src: LongPollId) -> &mut Self {
        self.put_u8(src.into());
        self
    }

    fn with_u8(&mut self, src: u8) -> &mut Self {
        self.put_u8(src);
        self
    }

    fn with_slice(&mut self, src: &[u8]) -> &mut Self {
        self.put_slice(src);
        self
    }

    fn with_u16(&mut self, src: u16) -> &mut Self {
        self.put_u16_le(src);
        self
    }

    fn with_u32(&mut self, src: u32) -> &mut Self {
        self.put_u32_le(src);
        self
    }

    fn with_bcd(&mut self, src: u64, dest_size: usize) -> &mut Self {
        self.with_slice(encode_bcd(src, dest_size).as_slice())
    }

    // fn with_progressive_level_data(&mut self, src: Vec<ProgressiveLevel>)  -> &mut Self {
    //     self.put_u8((Self::PROGRESSIVE_LEVEL_AMOUNT_SIZE * src.len()) as u8);
    //
    //     for progressive_level in src {
    //         self.put_u8(progressive_level.level as u8);
    //         self.put_slice(encode_bcd(progressive_level.amount, Self::PROGRESSIVE_LEVEL_AMOUNT_BCD_SIZE).as_slice());
    //     }
    //     self
    // }

    fn with_crc(&mut self) ->  &mut Self;

    //
    // fn with_crc(&mut self) ->  &mut Self {
    //     let crc = calc_crc_iter(self.into_iter());
    //     self.with_u16(crc)
    // }
}

pub trait SasSerialMessageReader : Buf {
    fn get_bcd(&mut self, bcd_len: usize) -> u64 {
        (0..bcd_len).fold(0, |acc, _ | (acc * 100) + decode_bcd_byte(self.get_u8()) as u64)
    }

    fn get_ascii(&mut self, len: usize) -> Vec<u8> {
        let mut buf = self.take(len);
        let mut dst = vec![];

        dst.put(&mut buf);
        dst
    }
}


impl SasSerialMessageReader for Bytes {}

impl SasSerialMessageReader for &[u8] {}
//impl SasSerialMessageReader for Vec<u8> {}

impl SasSerialMessageBuilder for Vec<u8> {
    fn with_crc(&mut self) ->  &mut Self {
        self.with_u16(calc_crc(self.as_slice()))
    }
}

pub struct SasSerialMessage {
    //buffer: Vec<u8>
}

pub enum SasReadError {
    CrcMismatch,
    AddressMismatch
}

pub trait SasSerial {
    const SAS_EGM_ADDRESS_OFFSET : usize = 0;
    const SAS_COMMAND_OFFSET : usize = 1;
    const SAS_BROADCAST_ADDR : u8 = 0;

    fn get_egm_address(&self) -> Option<u8>;
    fn get_long_poll(&self) -> Option<LongPollId>;
    fn get_embedded_crc(&self) -> u16;
    fn get_payload_crc(&self) -> u16;
    fn get_payload(&self) -> &[u8];
    fn is_valid_crc(&self) -> bool;
}

impl SasSerial for &[u8] {
    fn get_egm_address(&self) -> Option<u8> {
        match self.len() > 1 {
            true => Some(self[Self::SAS_EGM_ADDRESS_OFFSET]),
            _ => None
        }
    }

    fn get_long_poll(&self) -> Option<LongPollId> {
        match self.len() > 2 {
            true => {
                let command = self[Self::SAS_COMMAND_OFFSET];
                match LongPollId::try_from(command) {
                    Ok(long_poll) => Some(long_poll),
                    Err(_) => None
                }
            },
            _ => None
        }
    }

    fn get_embedded_crc(&self) -> u16 {
        let len = self.len();
        u16::from_le_bytes([self[len-2], self[len-1]])
    }

    fn get_payload_crc(&self) -> u16 {
        calc_crc(self.get_payload())
    }

    fn get_payload(&self) -> &[u8] {
        &self[..(self.len() - 2)]
    }

    fn is_valid_crc(&self) -> bool {
        self.get_embedded_crc() == self.get_embedded_crc()
    }
}

impl SasSerial for Vec<u8> {
    fn get_egm_address(&self) -> Option<u8> {
        match self.len() > 1 {
            true => Some(self[Self::SAS_EGM_ADDRESS_OFFSET]),
            _ => None
        }
    }

    fn get_long_poll(&self) -> Option<LongPollId> {
        match self.len() > 2 {
            true => {
                let command = self[Self::SAS_COMMAND_OFFSET];
                match LongPollId::try_from(command) {
                    Ok(long_poll) => Some(long_poll),
                    Err(_) => None
                }
            },
            _ => None
        }
    }

    fn get_embedded_crc(&self) -> u16 {
        let len = self.len();
        u16::from_le_bytes([self[len-2], self[len-1]])
    }

    fn get_payload_crc(&self) -> u16 {
        calc_crc(self.get_payload())
    }

    fn get_payload(&self) -> &[u8] {
        &self[..(self.len() - 2)]
    }

    fn is_valid_crc(&self) -> bool {
        self.get_embedded_crc() == self.get_embedded_crc()
    }
}

impl SasSerialMessage {
    const SAS_EGM_ADDRESS_OFFSET: usize = 0;
    // const SAS_COMMAND_OFFSET: usize = 1;
    const SAS_BROADCAST_ADDR: u8 = 0;

    pub fn embedded_crc(src: &[u8]) -> u16 {
        let len = src.len();
        u16::from_le_bytes([src[len - 2], src[len - 1]])
    }

    pub fn is_valid_embedded_crc(src: &[u8]) -> bool {
        let len = src.len();
        let payload_slice = &src[..(len - 3)];
        let _crc_slice = &src[(len - 3)..];

        let embedded_crc = SasSerialMessage::embedded_crc(src);
        let calculated_crc = calc_crc(payload_slice);

        calculated_crc == embedded_crc
    }

    pub fn is_valid_address(machine_id: u8, src: &[u8]) -> bool {
        let embedded_machine_addr = src[SasSerialMessage::SAS_EGM_ADDRESS_OFFSET];
        embedded_machine_addr == SasSerialMessage::SAS_BROADCAST_ADDR || embedded_machine_addr == machine_id
    }

    pub fn is_valid_sas_message(machine_id: u8, src: &[u8]) -> bool {
        if src.len() > 3 {
            let embedded_machine_addr = src[SasSerialMessage::SAS_EGM_ADDRESS_OFFSET];
            embedded_machine_addr == SasSerialMessage::SAS_BROADCAST_ADDR || embedded_machine_addr == machine_id
        } else {
            false
        }
    }

    pub fn new(_src: Vec<u8>) -> SasSerialMessage {
        SasSerialMessage { /* buffer: src */ }
    }

    pub fn from_slice(_machine_id: u8, _src: &[u8]) -> Result<SasSerialMessage, SasReadError> {
        Err(SasReadError::CrcMismatch)
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    const EGM_ID: u8 = 0x42;

    #[test]
    fn test_lp_startup() {
        let control_gaming_machine_startup = vec![0x42, 0x02, 0xC4, 0x56];

        assert_eq!(control_gaming_machine_startup.get_egm_address().unwrap(), EGM_ID);
        assert_eq!(control_gaming_machine_startup.get_long_poll().unwrap(), LongPollId::Startup);
        assert!(control_gaming_machine_startup.is_valid_crc());

        let message =  vec![EGM_ID].with_long_poll(LongPollId::Startup).with_crc().to_vec();
        assert_eq!(message, control_gaming_machine_startup);
    }

    #[test]
    fn test_lp_send_handpay_information() {
        let send_handpay_information = vec![0x42, 0x1b, 0x84, 0xdb];

        assert_eq!(send_handpay_information.get_egm_address().unwrap(), EGM_ID);
        assert_eq!(send_handpay_information.get_long_poll().unwrap(), LongPollId::SendHandpayInformation);
        assert!(send_handpay_information.is_valid_crc());

        let message =  vec![EGM_ID].with_long_poll(LongPollId::SendHandpayInformation).with_crc().to_vec();
        assert_eq!(message, send_handpay_information);
    }

    #[test]
    fn test_lp_send_gaming_machine_id_and_information() {
        let send_gaming_machine_id_and_information = vec![0x42, 0x1f, 0xa0, 0x9d];

        assert_eq!(send_gaming_machine_id_and_information.get_egm_address().unwrap(), EGM_ID);
        assert_eq!(send_gaming_machine_id_and_information.get_long_poll().unwrap(), LongPollId::SendGamingMachineIdAndInformation);
        assert!(send_gaming_machine_id_and_information.is_valid_crc());

        let message =  vec![EGM_ID].with_long_poll(LongPollId::SendGamingMachineIdAndInformation).with_crc().to_vec();
        assert_eq!(message, send_gaming_machine_id_and_information);
    }

    #[test]
    fn test_lp_send_selected_meters_for_game_n() {
        let send_selected_meters_for_game_n = vec![0x42, 0x2f, 0x08, 0x00, 0x06, 0x01, 0x02, 0x03, 0xdc, 0x25];

        assert_eq!(send_selected_meters_for_game_n.get_egm_address().unwrap(), EGM_ID);
        assert_eq!(send_selected_meters_for_game_n.get_long_poll().unwrap(), LongPollId::SendSelectedMetersForGameN);
        assert!(send_selected_meters_for_game_n.is_valid_crc());

        let message =  vec![EGM_ID].with_long_poll(LongPollId::SendSelectedMetersForGameN)
            .with_u8(08)
            .with_bcd(6, 2)
            .with_u8(1)
            .with_u8(2)
            .with_u8(3)
            .with_crc().to_vec();

        assert_eq!(message, send_selected_meters_for_game_n);
    }

    #[test]
    fn test_bcd() {
        let value = 123456;
        let bcd_size = 6;
        let mut v:Vec<u8> = vec![];

        let mut buf = &v.with_bcd(value, bcd_size)[..];
        let value_read = buf.get_bcd(bcd_size);

        assert_eq!(value, value_read);

        // let mut buf3:Vec<u8> = vec![0x12, 0x34];
        // let mut buf2 = &buf3[..];
        //
        // let bcd = buf3.get_bcd(2);
        // assert_eq!(bcd, 1234);

    }
}